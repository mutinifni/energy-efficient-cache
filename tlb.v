`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:51:07 04/09/2016 
// Design Name: 
// Module Name:    tlb 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module D_ff_VA (input clk, input reset, input regWrite,input init,output reg q);
	always @ (negedge clk)
	begin
	if(reset==1)
		q=0;
	else if(regWrite==1)
		q=init;
	end
endmodule

module VA_register(input clk,input reset,input regWrite,input [41:0] page_offset, output [41:0] page_offset_out);
		D_ff_VA dff_VA0(clk, reset, regWrite,page_offset[0],page_offset_out[0]);
		D_ff_VA dff_VA1(clk, reset, regWrite,page_offset[1],page_offset_out[1]);
		D_ff_VA dff_VA2(clk, reset, regWrite,page_offset[2],page_offset_out[2]);
		D_ff_VA dff_VA3(clk, reset, regWrite,page_offset[3],page_offset_out[3]);
		
		D_ff_VA dff_VA4(clk, reset, regWrite,page_offset[4],page_offset_out[4]);
		D_ff_VA dff_VA5(clk, reset, regWrite,page_offset[5],page_offset_out[5]);
		D_ff_VA dff_VA6(clk, reset, regWrite,page_offset[6],page_offset_out[6]);
		D_ff_VA dff_VA7(clk, reset, regWrite,page_offset[7],page_offset_out[7]);
		
		D_ff_VA dff_VA8(clk, reset, regWrite,page_offset[8],page_offset_out[8]);
		D_ff_VA dff_VA9(clk, reset, regWrite,page_offset[9],page_offset_out[9]);
		D_ff_VA dff_VA10(clk, reset, regWrite,page_offset[10],page_offset_out[10]);
		D_ff_VA dff_VA11(clk, reset, regWrite,page_offset[11],page_offset_out[11]);
		
		D_ff_VA dff_VA12(clk, reset, regWrite,page_offset[12],page_offset_out[12]);
		D_ff_VA dff_VA13(clk, reset, regWrite,page_offset[13],page_offset_out[13]);
		D_ff_VA dff_VA14(clk, reset, regWrite,page_offset[14],page_offset_out[14]);
		D_ff_VA dff_VA15(clk, reset, regWrite,page_offset[15],page_offset_out[15]);

		D_ff_VA dff_VA16(clk, reset, regWrite,page_offset[16],page_offset_out[16]);
		D_ff_VA dff_VA17(clk, reset, regWrite,page_offset[17],page_offset_out[17]);
		D_ff_VA dff_VA18(clk, reset, regWrite,page_offset[18],page_offset_out[18]);
		D_ff_VA dff_VA19(clk, reset, regWrite,page_offset[19],page_offset_out[19]);
		
		D_ff_VA dff_VA20(clk, reset, regWrite,page_offset[20],page_offset_out[20]);
		D_ff_VA dff_VA21(clk, reset, regWrite,page_offset[21],page_offset_out[21]);
		D_ff_VA dff_VA22(clk, reset, regWrite,page_offset[22],page_offset_out[22]);
		D_ff_VA dff_VA23(clk, reset, regWrite,page_offset[23],page_offset_out[23]);

		D_ff_VA dff_VA24(clk, reset, regWrite,page_offset[24],page_offset_out[24]);
		D_ff_VA dff_VA25(clk, reset, regWrite,page_offset[25],page_offset_out[25]);
		D_ff_VA dff_VA26(clk, reset, regWrite,page_offset[26],page_offset_out[26]);
		D_ff_VA dff_VA27(clk, reset, regWrite,page_offset[27],page_offset_out[27]);
		
		D_ff_VA dff_VA28(clk, reset, regWrite,page_offset[28],page_offset_out[28]);
		D_ff_VA dff_VA29(clk, reset, regWrite,page_offset[29],page_offset_out[29]);
		D_ff_VA dff_VA30(clk, reset, regWrite,page_offset[30],page_offset_out[30]);
		D_ff_VA dff_VA31(clk, reset, regWrite,page_offset[31],page_offset_out[31]);

		D_ff_VA dff_VA32(clk, reset, regWrite,page_offset[32],page_offset_out[32]);
		D_ff_VA dff_VA33(clk, reset, regWrite,page_offset[33],page_offset_out[33]);
		D_ff_VA dff_VA34(clk, reset, regWrite,page_offset[34],page_offset_out[34]);
		D_ff_VA dff_VA35(clk, reset, regWrite,page_offset[35],page_offset_out[35]);
		
		D_ff_VA dff_VA36(clk, reset, regWrite,page_offset[36],page_offset_out[36]);
		D_ff_VA dff_VA37(clk, reset, regWrite,page_offset[37],page_offset_out[37]);
		D_ff_VA dff_VA38(clk, reset, regWrite,page_offset[38],page_offset_out[38]);
		D_ff_VA dff_VA39(clk, reset, regWrite,page_offset[39],page_offset_out[39]);

		D_ff_VA dff_VA40(clk, reset, regWrite,page_offset[40],page_offset_out[40]);
		D_ff_VA dff_VA41(clk, reset, regWrite,page_offset[41],page_offset_out[41]);
endmodule

module protection_register(input clk,input reset,input regWrite,input [4:0] protection_in, output [4:0] protection_out);
		D_ff_VA dff_P0(clk, reset, regWrite,protection_in[0],protection_out[0]);
		D_ff_VA dff_P1(clk, reset, regWrite,protection_in[1],protection_out[1]);
		D_ff_VA dff_P2(clk, reset, regWrite,protection_in[2],protection_out[2]);
		D_ff_VA dff_P3(clk, reset, regWrite,protection_in[3],protection_out[3]);
		D_ff_VA dff_P4(clk, reset, regWrite,protection_in[4],protection_out[4]);
endmodule 

module D_ff_TLB(input clk, input reset, input regWrite, input decOut1b,input init, input d, output reg q);
	always @ (negedge clk)
	begin
	if(reset==1)
		q=init;
	else
		if(regWrite == 1 && decOut1b==1) begin q=d; end
	end
endmodule
	
module TLB_register(input clk,input reset,input regWrite,input decOut1b,input [61:0]init, input [61:0] data_in, output [61:0] data_out);
	D_ff_TLB dff_tlb0(clk, reset, regWrite, decOut1b, init[0], data_in[0], data_out[0]);
	D_ff_TLB dff_tlb1(clk, reset, regWrite, decOut1b, init[1], data_in[1], data_out[1]);
	D_ff_TLB dff_tlb2(clk, reset, regWrite, decOut1b, init[2], data_in[2], data_out[2]);
	D_ff_TLB dff_tlb3(clk, reset, regWrite, decOut1b, init[3], data_in[3], data_out[3]);

	D_ff_TLB dff_tlb4(clk, reset, regWrite, decOut1b, init[4], data_in[4], data_out[4]);
	D_ff_TLB dff_tlb5(clk, reset, regWrite, decOut1b, init[5], data_in[5], data_out[5]);
	D_ff_TLB dff_tlb6(clk, reset, regWrite, decOut1b, init[6], data_in[6], data_out[6]);
	D_ff_TLB dff_tlb7(clk, reset, regWrite, decOut1b, init[7], data_in[7], data_out[7]);

	D_ff_TLB dff_tlb8(clk, reset, regWrite, decOut1b, init[8], data_in[8], data_out[8]);
	D_ff_TLB dff_tlb9(clk, reset, regWrite, decOut1b, init[9], data_in[9], data_out[9]);
	D_ff_TLB dff_tlb10(clk, reset, regWrite, decOut1b, init[10], data_in[10], data_out[10]);
	D_ff_TLB dff_tlb11(clk, reset, regWrite, decOut1b, init[11], data_in[11], data_out[11]);
	
	D_ff_TLB dff_tlb12(clk, reset, regWrite, decOut1b, init[12], data_in[12], data_out[12]);
	D_ff_TLB dff_tlb13(clk, reset, regWrite, decOut1b, init[13], data_in[13], data_out[13]);
	D_ff_TLB dff_tlb14(clk, reset, regWrite, decOut1b, init[14], data_in[14], data_out[14]);
	D_ff_TLB dff_tlb15(clk, reset, regWrite, decOut1b, init[15], data_in[15], data_out[15]);
	
	D_ff_TLB dff_tlb16(clk, reset, regWrite, decOut1b, init[16], data_in[16], data_out[16]);
	D_ff_TLB dff_tlb17(clk, reset, regWrite, decOut1b, init[17], data_in[17], data_out[17]);
	D_ff_TLB dff_tlb18(clk, reset, regWrite, decOut1b, init[18], data_in[18], data_out[18]);
	D_ff_TLB dff_tlb19(clk, reset, regWrite, decOut1b, init[19], data_in[19], data_out[19]);

	D_ff_TLB dff_tlb20(clk, reset, regWrite, decOut1b, init[20], data_in[20], data_out[20]);
	D_ff_TLB dff_tlb21(clk, reset, regWrite, decOut1b, init[21], data_in[21], data_out[21]);
	D_ff_TLB dff_tlb22(clk, reset, regWrite, decOut1b, init[22], data_in[22], data_out[22]);
	D_ff_TLB dff_tlb23(clk, reset, regWrite, decOut1b, init[23], data_in[23], data_out[23]);
	
	D_ff_TLB dff_tlb24(clk, reset, regWrite, decOut1b, init[24], data_in[24], data_out[24]);
	D_ff_TLB dff_tlb25(clk, reset, regWrite, decOut1b, init[25], data_in[25], data_out[25]);
	D_ff_TLB dff_tlb26(clk, reset, regWrite, decOut1b, init[26], data_in[26], data_out[26]);
	D_ff_TLB dff_tlb27(clk, reset, regWrite, decOut1b, init[27], data_in[27], data_out[27]);
	
	D_ff_TLB dff_tlb28(clk, reset, regWrite, decOut1b, init[28], data_in[28], data_out[28]);
	D_ff_TLB dff_tlb29(clk, reset, regWrite, decOut1b, init[29], data_in[29], data_out[29]);
	D_ff_TLB dff_tlb30(clk, reset, regWrite, decOut1b, init[30], data_in[30], data_out[30]);
	D_ff_TLB dff_tlb31(clk, reset, regWrite, decOut1b, init[31], data_in[31], data_out[31]);
	
	D_ff_TLB dff_tlb32(clk, reset, regWrite, decOut1b, init[32], data_in[32], data_out[32]);
	D_ff_TLB dff_tlb33(clk, reset, regWrite, decOut1b, init[33], data_in[33], data_out[33]);
	D_ff_TLB dff_tlb34(clk, reset, regWrite, decOut1b, init[34], data_in[34], data_out[34]);
	D_ff_TLB dff_tlb35(clk, reset, regWrite, decOut1b, init[35], data_in[35], data_out[35]);

	D_ff_TLB dff_tlb36(clk, reset, regWrite, decOut1b, init[36], data_in[36], data_out[36]);
	D_ff_TLB dff_tlb37(clk, reset, regWrite, decOut1b, init[37], data_in[37], data_out[37]);
	D_ff_TLB dff_tlb38(clk, reset, regWrite, decOut1b, init[38], data_in[38], data_out[38]);
	D_ff_TLB dff_tlb39(clk, reset, regWrite, decOut1b, init[39], data_in[39], data_out[39]);
	
	D_ff_TLB dff_tlb40(clk, reset, regWrite, decOut1b, init[40], data_in[40], data_out[40]);
	D_ff_TLB dff_tlb41(clk, reset, regWrite, decOut1b, init[41], data_in[41], data_out[41]);
	D_ff_TLB dff_tlb42(clk, reset, regWrite, decOut1b, init[42], data_in[42], data_out[42]);
	D_ff_TLB dff_tlb43(clk, reset, regWrite, decOut1b, init[43], data_in[43], data_out[43]);
	
	D_ff_TLB dff_tlb44(clk, reset, regWrite, decOut1b, init[44], data_in[44], data_out[44]);
	D_ff_TLB dff_tlb45(clk, reset, regWrite, decOut1b, init[45], data_in[45], data_out[45]);
	D_ff_TLB dff_tlb46(clk, reset, regWrite, decOut1b, init[46], data_in[46], data_out[46]);
	D_ff_TLB dff_tlb47(clk, reset, regWrite, decOut1b, init[47], data_in[47], data_out[47]);
	
	D_ff_TLB dff_tlb48(clk, reset, regWrite, decOut1b, init[48], data_in[48], data_out[48]);
	D_ff_TLB dff_tlb49(clk, reset, regWrite, decOut1b, init[49], data_in[49], data_out[49]);
	D_ff_TLB dff_tlb50(clk, reset, regWrite, decOut1b, init[50], data_in[50], data_out[50]);
	D_ff_TLB dff_tlb51(clk, reset, regWrite, decOut1b, init[51], data_in[51], data_out[51]);

	D_ff_TLB dff_tlb52(clk, reset, regWrite, decOut1b, init[52], data_in[52], data_out[52]);
	D_ff_TLB dff_tlb53(clk, reset, regWrite, decOut1b, init[53], data_in[53], data_out[53]);
	D_ff_TLB dff_tlb54(clk, reset, regWrite, decOut1b, init[54], data_in[54], data_out[54]);
	D_ff_TLB dff_tlb55(clk, reset, regWrite, decOut1b, init[55], data_in[55], data_out[55]);
	
	D_ff_TLB dff_tlb56(clk, reset, regWrite, decOut1b, init[56], data_in[56], data_out[56]);
	D_ff_TLB dff_tlb57(clk, reset, regWrite, decOut1b, init[57], data_in[57], data_out[57]);
	D_ff_TLB dff_tlb58(clk, reset, regWrite, decOut1b, init[58], data_in[58], data_out[58]);
	D_ff_TLB dff_tlb59(clk, reset, regWrite, decOut1b, init[59], data_in[59], data_out[59]);
	
	D_ff_TLB dff_tlb60(clk, reset, regWrite, decOut1b, init[60], data_in[60], data_out[60]);
	D_ff_TLB dff_tlb61(clk, reset, regWrite, decOut1b, init[61], data_in[61], data_out[61]);
	
endmodule

module TLB_set(input clk,input reset,input regWrite,input [31:0]decOut, input [61:0] data_in, 
				output [61:0] out0,out1,out2,out3,out4,out5,out6,out7,out8,out9,out10,out11,out12,out13,out14,out15,out16,out17,out18,out19,out20,out21,out22,out23,out24,out25,out26,out27,out28,out29,out30,out31);
	
	TLB_register tlb0(clk,reset,regWrite, decOut[0],62'h2078A824ffffffe5, data_in, out0);
	TLB_register tlb1(clk,reset,regWrite, decOut[1],62'd0, data_in, out1);
	TLB_register tlb2(clk,reset,regWrite, decOut[2],62'd0, data_in, out2);
	TLB_register tlb3(clk,reset,regWrite, decOut[3],62'd0, data_in, out3);

	TLB_register tlb4(clk,reset,regWrite, decOut[4],62'd0, data_in, out4);
	TLB_register tlb5(clk,reset,regWrite, decOut[5],62'd0, data_in, out5);
	TLB_register tlb6(clk,reset,regWrite, decOut[6],62'd0, data_in, out6);
	TLB_register tlb7(clk,reset,regWrite, decOut[7],62'b1_100010101000011111000100011101_00000000000000000000000111_01000, data_in, out7);
	
	TLB_register tlb8(clk,reset,regWrite, decOut[8],62'd0, data_in, out8);
	TLB_register tlb9(clk,reset,regWrite, decOut[9],62'd0, data_in, out9);
	TLB_register tlb10(clk,reset,regWrite, decOut[10],62'd0, data_in, out10);
	TLB_register tlb11(clk,reset,regWrite, decOut[11],62'd0, data_in, out11);

	TLB_register tlb12(clk,reset,regWrite, decOut[12],62'd0, data_in, out12);
	TLB_register tlb13(clk,reset,regWrite, decOut[13],62'd0, data_in, out13);
	TLB_register tlb14(clk,reset,regWrite, decOut[14],62'd0, data_in, out14);
	TLB_register tlb15(clk,reset,regWrite, decOut[15],62'd0, data_in, out15);
	
	TLB_register tlb16(clk,reset,regWrite, decOut[16],62'd0, data_in, out16);
	TLB_register tlb17(clk,reset,regWrite, decOut[17],62'd0, data_in, out17);
	TLB_register tlb18(clk,reset,regWrite, decOut[18],62'd0, data_in, out18);
	TLB_register tlb19(clk,reset,regWrite, decOut[19],62'd0, data_in, out19);

	TLB_register tlb20(clk,reset,regWrite, decOut[20],62'd0, data_in, out20);
	TLB_register tlb21(clk,reset,regWrite, decOut[21],62'd0, data_in, out21);
	TLB_register tlb22(clk,reset,regWrite, decOut[22],62'd0, data_in, out22);
	TLB_register tlb23(clk,reset,regWrite, decOut[23],62'd0, data_in, out23);
	
	TLB_register tlb24(clk,reset,regWrite, decOut[24],62'd0, data_in, out24);
	TLB_register tlb25(clk,reset,regWrite, decOut[25],62'd0, data_in, out25);
	TLB_register tlb26(clk,reset,regWrite, decOut[26],62'd0, data_in, out26);
	TLB_register tlb27(clk,reset,regWrite, decOut[27],62'd0, data_in, out27);

	TLB_register tlb28(clk,reset,regWrite, decOut[28],62'd0, data_in, out28);
	TLB_register tlb29(clk,reset,regWrite, decOut[29],62'd0, data_in, out29);
	TLB_register tlb30(clk,reset,regWrite, decOut[30],62'd0, data_in, out30);
	TLB_register tlb31(clk,reset,regWrite, decOut[31],62'd0, data_in, out31);

endmodule

module decoder5to32_tlb(input [4:0] destReg, output reg [31:0] decOut );
	always@(destReg)
	case(destReg)
			5'd0: decOut=32'b0000_0000_0000_0000_0000_0000_0000_0001; 
			5'd1: decOut=32'b0000_0000_0000_0000_0000_0000_0000_0010; 
			5'd2: decOut=32'b0000_0000_0000_0000_0000_0000_0000_0100; 
			5'd3: decOut=32'b0000_0000_0000_0000_0000_0000_0000_1000; 
			
			5'd4: decOut=32'b0000_0000_0000_0000_0000_0000_0001_0000; 
			5'd5: decOut=32'b0000_0000_0000_0000_0000_0000_0010_0000; 
			5'd6: decOut=32'b0000_0000_0000_0000_0000_0000_0100_0000; 
			5'd7: decOut=32'b0000_0000_0000_0000_0000_0000_1000_0000; 
			
			5'd8: decOut=32'b0000_0000_0000_0000_0000_0001_0000_0000; 
			5'd9: decOut=32'b0000_0000_0000_0000_0000_0010_0000_0000; 
			5'd10: decOut=32'b0000_0000_0000_0000_0000_0100_0000_0000; 
			5'd11: decOut=32'b0000_0000_0000_0000_0000_1000_0000_0000; 
			
			5'd12: decOut=32'b0000_0000_0000_0000_0001_0000_0000_0000; 
			5'd13: decOut=32'b0000_0000_0000_0000_0010_0000_0000_0000; 
			5'd14: decOut=32'b0000_0000_0000_0000_0100_0000_0000_0000; 
			5'd15: decOut=32'b0000_0000_0000_0000_1000_0000_0000_0000; 
			
			5'd16: decOut=32'b0000_0000_0000_0001_0000_0000_0000_0001; 
			5'd17: decOut=32'b0000_0000_0000_0010_0000_0000_0000_0000; 
			5'd18: decOut=32'b0000_0000_0000_0100_0000_0000_0000_0000; 
			5'd19: decOut=32'b0000_0000_0000_1000_0000_0000_0000_0000; 
			
			5'd20: decOut=32'b0000_0000_0001_0000_0000_0000_0000_0000; 
			5'd21: decOut=32'b0000_0000_0010_0000_0000_0000_0000_0000; 
			5'd22: decOut=32'b0000_0000_0100_0000_0000_0000_0000_0000; 
			5'd23: decOut=32'b0000_0000_1000_0000_0000_0000_0000_0000; 
			
			5'd24: decOut=32'b0000_0001_0000_0000_0000_0000_0000_0000; 
			5'd25: decOut=32'b0000_0010_0000_0000_0000_0000_0000_0000; 
			5'd26: decOut=32'b0000_0100_0000_0000_0000_0000_0000_0000; 
			5'd27: decOut=32'b0000_1000_0000_0000_0000_0000_0000_0000; 
			
			5'd28: decOut=32'b0001_0000_0000_0000_0000_0000_0000_0000; 
			5'd29: decOut=32'b0010_0000_0000_0000_0000_0000_0000_0000; 
			5'd30: decOut=32'b0100_0000_0000_0000_0000_0000_0000_0000; 
			5'd31: decOut=32'b1000_0000_0000_0000_0000_0000_0000_0000; 
			
	endcase

endmodule

module ctrl_ckt_cache (input clk, input reset, input hit_miss, input hit1c, input hit2c, input CPUWriteSignal, input CPURead,
						output reg regWrite,	output reg PA_write, output reg TLB_write ,
                       output reg chooseOffset, output reg FSM, output reg [3:0] writeOffset, output reg writeLRU,
                       output reg writeReg, output reg MRUWrite, output reg isValid, output reg L2Write, output reg way_dec_mux_sel, output reg writeValid);
  reg [4:0] state;
  always @ (negedge clk)
  begin
    if (reset)
      begin
		regWrite=1'b0;
        PA_write=1'b0;
		TLB_write=1'b0;
		writeValid = 1'b0;
        FSM = 1'b0;
        state = 20;
        writeOffset = 4'b0000;
        way_dec_mux_sel = 1'b0;
        isValid = 1'b0;
        MRUWrite = 1'b0;
        writeReg = 1'b0;
        writeLRU = 1'b0;
        L2Write = 1'b0;
      end
    else
      begin
        case(state)
		  20:
		  
			begin
			regWrite=1'b1;
			PA_write=1'b0;
			TLB_write=1'b0;
			state=21;
			end
				
		  21:
			begin
			regWrite=1'b0;
			PA_write=1'b0;
			TLB_write=1'b0;
			state=22;
			end
					
		  22:
			begin
		    regWrite=1'b0;
			if(hit_miss==1)
				begin	
				TLB_write=1'b0;
				PA_write=1'b1;
				state=0;
				end
			else
				begin	
				TLB_write=1'b1;
				PA_write=1'b0;
				state=23;
				end
			end
		  23:
			begin
			TLB_write=1'b0;
			regWrite=1'b0;
			PA_write=1'b1;
			state=24;
			end
			
		  24:
		  begin
			TLB_write=1'b0;
			regWrite=1'b0;
			PA_write=1'b0;
			state=0;
			end
		
          0:
          begin
            FSM = 1'b0;
            chooseOffset = 1'b0;
            writeOffset = 4'b0000;
            MRUWrite = 1'b0;
            writeLRU = 1'b0;
            L2Write = 1'b0;
				writeValid = 1'b0;
            state = 18;
            if (hit1c == 1'b1)
              begin
              isValid = CPURead;
              writeReg = CPUWriteSignal;
              way_dec_mux_sel = 1'b0;
              end
            else
              begin
              FSM = 1'b1;
              isValid = 1'b0;
              MRUWrite = 1'b1;
              way_dec_mux_sel = 1'b0;
              writeReg = 1'b0;
              end
          end
          
          19:
          begin
            writeLRU = 1'b0;
            writeReg = 1'b0;
            way_dec_mux_sel = 1'b0;
            state = 20;
          end
          
          18:
          begin
            if (hit1c == 1'b1)
              begin
              isValid = 1'b0;
              MRUWrite = 1'b0;
              way_dec_mux_sel = 1'b0;
              if (CPUWriteSignal == 1'b1)
                begin
                  state = 19;
                end
              else
                begin
                  state = 20;
                end
              writeReg = 1'b0;
              end
            else
              begin
              if (hit2c == 1'b1)
                begin
                state = 1;
                writeLRU = 1'b1;
                FSM = 1'b0;
                isValid = CPURead;
                writeReg = CPUWriteSignal;
                MRUWrite = 1'b1;
                way_dec_mux_sel = 1'b0;
                end
              else
                begin
                L2Write = 1'b1;
                chooseOffset = 1'b1;
                way_dec_mux_sel = 1'b1;
                writeOffset = 4'b0000;
                writeReg = 1'b1;
                state = 2;
                FSM = 1'b1;
                end
              end
          end
          
          1:
          begin
            FSM = 1'b0;
            writeLRU = 1'b0;
            way_dec_mux_sel = 1'b0;
            MRUWrite = 1'b0;
            state = 20;
            writeReg = 1'b0;
            isValid = 1'b0;
            L2Write = 1'b0;
          end
          
          2:
          begin
            isValid = 1'b0;
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b0001;
            MRUWrite = 1'b0;
            L2Write = 1'b0;
            state = 3;
          end
          
          3:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b0010;
            MRUWrite = 1'b0;
            state = 4;
          end
          
          4:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b0011;
            MRUWrite = 1'b0;
            state = 5;
          end
          
          5:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b0100;
            MRUWrite = 1'b0;
            state = 6;
          end
          
          6:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b0101;
            MRUWrite = 1'b0;
            state = 7;
          end
          
          7:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b0110;
            MRUWrite = 1'b0;
            state = 8;
          end
          
          8:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b0111;
            MRUWrite = 1'b0;
            state = 9;
          end
          
          9:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b1000;
            MRUWrite = 1'b0;
            state = 10;
          end
          
          10:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b1001;
            MRUWrite = 1'b0;
            state = 11;
          end
          
          11:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b1010;
            MRUWrite = 1'b0;
            state = 12;
          end
          
          12:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b1011;
            MRUWrite = 1'b0;
            state = 13;
          end
          
          13:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b1100;
            MRUWrite = 1'b0;
            state = 14;
          end
          
          14:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b1101;
            MRUWrite = 1'b0;
            state = 15;
          end
          
          15:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b1110;
            MRUWrite = 1'b0;
            state = 16;
          end
          
          16:
          begin
            chooseOffset = 1'b1;
            writeLRU = 1'b0;
            writeReg = 1'b1;
            writeOffset = 4'b1111;
            MRUWrite = 1'b0;
            state = 25;
				writeValid = 1'b1;
          end
          
          17:
          begin
            chooseOffset = 1'b0;
				writeValid = 1'b0;
            writeLRU = 1'b0;
            writeReg = 1'b0;
            writeOffset = 4'b0000;
            MRUWrite = 1'b0;
            FSM = 1'b1;
            state = 1;
            L2Write = 1'b0;
            if (hit2c == 1'b1)
                begin
                state = 1;
                FSM = 1'b0;
                isValid = CPURead;
                writeLRU = 1'b1;
                writeReg = CPUWriteSignal;
                way_dec_mux_sel = 1'b0;
                end
            else
                begin
                L2Write = 1'b1;
                chooseOffset = 1'b1;
                writeOffset = 4'b0000;
                state = 2;
                FSM = 1'b1;
                end
          end
			 
			 25:
			 begin
				chooseOffset = 1'b0;
				writeValid = 1'b0;
            writeLRU = 1'b0;
            writeReg = 1'b0;
            writeOffset = 4'b0000;
            MRUWrite = 1'b1;
            FSM = 1'b1;
				state = 17;
				L2Write = 1'b0;
			 end
			 
      endcase
    end
  end
endmodule

module comparator_tlb(input [29:0]in1,input [29:0]in2, output reg equal);
	always@(in1 or in2)
	begin
		if(in1==in2)
			begin 	equal=1;	end
		else
			begin 	equal=0;	end
	end
endmodule

module comparator_exception_tlb(input [4:0]in1,input [4:0]in2, output reg less_equal);
	always@(in1 or in2)
	begin
		if(in1==in2 || in1 < in2)
			begin 	less_equal=1;	end
		else
			begin 	less_equal=0;	end
	end
endmodule

module mux_frame32to1(input [25:0] outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7,outR8,outR9,outR10,outR11,outR12,outR13,outR14,outR15,outR16,outR17,outR18,outR19,outR20,outR21,outR22,outR23,outR24,outR25,outR26,outR27,outR28,outR29,outR30,outR31,
                      input [4:0] Sel, output reg [25:0] outBus );
	always@(outR0 or outR1 or outR2 or outR3 or outR4 or outR5 or outR6 or outR7 or outR8 or outR9 or outR10 or outR11 or outR12 or outR13 or outR14 or outR15 or outR16 or outR17 or outR18 or outR19 or outR20 or outR21 or outR22 or outR23 or outR24 or outR25 or outR26 or outR27 or outR28 or outR29 or outR30 or outR31 or Sel)
	case (Sel)
				5'd0: outBus=outR0;
				5'd1: outBus=outR1;
				5'd2: outBus=outR2;
				5'd3: outBus=outR3;
				
				5'd4: outBus=outR4;
				5'd5: outBus=outR5;
				5'd6: outBus=outR6;
				5'd7: outBus=outR7;
				
				5'd8: outBus=outR8;
				5'd9: outBus=outR9;
				5'd10: outBus=outR10;
				5'd11: outBus=outR11;
				
				5'd12: outBus=outR12;
				5'd13: outBus=outR13;
				5'd14: outBus=outR14;
				5'd15: outBus=outR15;
				
				5'd16: outBus=outR16;
				5'd17: outBus=outR17;
				5'd18: outBus=outR18;
				5'd19: outBus=outR19;
				
				5'd20: outBus=outR20;
				5'd21: outBus=outR21;
				5'd22: outBus=outR22;
				5'd23: outBus=outR23;
				
				5'd24: outBus=outR24;
				5'd25: outBus=outR25;
				5'd26: outBus=outR26;
				5'd27: outBus=outR27;
				
				5'd28: outBus=outR28;
				5'd29: outBus=outR29;
				5'd30: outBus=outR30;
				5'd31: outBus=outR31;
				
	endcase

endmodule

module mux_protection32to1(input [4:0] outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7,outR8,outR9,outR10,outR11,outR12,outR13,outR14,outR15,outR16,outR17,outR18,outR19,outR20,outR21,outR22,outR23,outR24,outR25,outR26,outR27,outR28,outR29,outR30,outR31,
                      input [4:0] Sel, output reg [4:0] outBus );
	always@(outR0 or outR1 or outR2 or outR3 or outR4 or outR5 or outR6 or outR7 or outR8 or outR9 or outR10 or outR11 or outR12 or outR13 or outR14 or outR15 or outR16 or outR17 or outR18 or outR19 or outR20 or outR21 or outR22 or outR23 or outR24 or outR25 or outR26 or outR27 or outR28 or outR29 or outR30 or outR31 or Sel)
	case (Sel)
				5'd0: outBus=outR0;
				5'd1: outBus=outR1;
				5'd2: outBus=outR2;
				5'd3: outBus=outR3;
				
				5'd4: outBus=outR4;
				5'd5: outBus=outR5;
				5'd6: outBus=outR6;
				5'd7: outBus=outR7;
				
				5'd8: outBus=outR8;
				5'd9: outBus=outR9;
				5'd10: outBus=outR10;
				5'd11: outBus=outR11;
				
				5'd12: outBus=outR12;
				5'd13: outBus=outR13;
				5'd14: outBus=outR14;
				5'd15: outBus=outR15;
				
				5'd16: outBus=outR16;
				5'd17: outBus=outR17;
				5'd18: outBus=outR18;
				5'd19: outBus=outR19;
				
				5'd20: outBus=outR20;
				5'd21: outBus=outR21;
				5'd22: outBus=outR22;
				5'd23: outBus=outR23;
				
				5'd24: outBus=outR24;
				5'd25: outBus=outR25;
				5'd26: outBus=outR26;
				5'd27: outBus=outR27;
				
				5'd28: outBus=outR28;
				5'd29: outBus=outR29;
				5'd30: outBus=outR30;
				5'd31: outBus=outR31;
				
	endcase

endmodule

module priority_encoder_tlb(input reset, input [31:0]andOut,output reg [4:0] LINE);
	always@(reset or andOut) 
		begin
			 if(reset)
					begin LINE = 5'd0; end
			 else
			   begin if(andOut[0] == 1) 
					begin LINE = 5'd0; end 
				else if(andOut[1] == 1) 
				   begin LINE = 5'd1; end 
				else if(andOut[2] == 1) 
				   begin LINE = 5'd2; end 
				else if(andOut[3] == 1) 
				   begin LINE = 5'd3; end 
					
				else if(andOut[4] == 1) 
				   begin LINE = 5'd4; end 
				else if(andOut[5] == 1) 
				   begin LINE = 5'd5; end 
				else if(andOut[6] == 1) 
				   begin LINE = 5'd6; end 
				else if(andOut[7] == 1) 
				   begin LINE = 5'd7; end 
					
				else if(andOut[8] == 1) 
				   begin LINE = 5'd8; end 
				else if(andOut[9] == 1) 
				   begin LINE = 5'd9; end 
				else if(andOut[10] == 1) 
				   begin LINE = 5'd10; end 
				else if(andOut[11] == 1) 
				   begin LINE = 5'd11; end
					
				else if(andOut[12] == 1) 
				   begin LINE = 5'd12; end 
				else if(andOut[13] == 1) 
				   begin LINE = 5'd13; end 
				else if(andOut[14] == 1) 
				   begin LINE = 5'd14; end 
				else if(andOut[15] == 1) 
				   begin LINE = 5'd15; end 
					
				else if(andOut[16] == 1) 
				   begin LINE = 5'd16; end 
				else if(andOut[17] == 1) 
				   begin LINE = 5'd17; end 
				else if(andOut[18] == 1) 
				   begin LINE = 5'd18; end 
				else if(andOut[19] == 1) 
				   begin LINE = 5'd19; end 
					
				else if(andOut[20] == 1) 
				   begin LINE = 5'd20; end 
				else if(andOut[21] == 1) 
				   begin LINE = 5'd21; end 
				else if(andOut[22] == 1) 
				   begin LINE = 5'd22; end 
				else if(andOut[23] == 1) 
				   begin LINE = 5'd23; end 
					
				else if(andOut[24] == 1) 
				   begin LINE = 5'd24; end 
				else if(andOut[25] == 1) 
				   begin LINE = 5'd25; end 
				else if(andOut[26] == 1) 
				   begin LINE = 5'd26; end 
				else if(andOut[27] == 1) 
				   begin LINE = 5'd27; end 
					
				else if(andOut[28] == 1) 
				   begin LINE = 5'd28; end 
				else if(andOut[29] == 1) 
				   begin LINE = 5'd29; end
				else if(andOut[30] == 1) 
				   begin LINE = 5'd30; end
				else if(andOut[31] == 1) 
				   begin LINE = 5'd31; end
			  end
		end

endmodule

module D_ff_PA (input clk, input reset, input regWrite,input init,output reg q);
	always @ (negedge clk)
	begin
	if(reset==1 )
		q=0;
	else if(regWrite==1)
		q=init;
	end
endmodule

module register_PA(input clk,input reset,input regWrite,input [37:0] frame_offset, output [37:0] frame_offset_out);
		D_ff_PA dff_PA0(clk, reset, regWrite,frame_offset[0],frame_offset_out[0]);
		D_ff_PA dff_PA1(clk, reset, regWrite,frame_offset[1],frame_offset_out[1]);
		D_ff_PA dff_PA2(clk, reset, regWrite,frame_offset[2],frame_offset_out[2]);
		D_ff_PA dff_PA3(clk, reset, regWrite,frame_offset[3],frame_offset_out[3]);
		
		D_ff_PA dff_PA4(clk, reset, regWrite,frame_offset[4],frame_offset_out[4]);
		D_ff_PA dff_PA5(clk, reset, regWrite,frame_offset[5],frame_offset_out[5]);
		D_ff_PA dff_PA6(clk, reset, regWrite,frame_offset[6],frame_offset_out[6]);
		D_ff_PA dff_PA7(clk, reset, regWrite,frame_offset[7],frame_offset_out[7]);
		
		D_ff_PA dff_PA8(clk, reset, regWrite,frame_offset[8],frame_offset_out[8]);
		D_ff_PA dff_PA9(clk, reset, regWrite,frame_offset[9],frame_offset_out[9]);
		D_ff_PA dff_PA10(clk, reset, regWrite,frame_offset[10],frame_offset_out[10]);
		D_ff_PA dff_PA11(clk, reset, regWrite,frame_offset[11],frame_offset_out[11]);
		
		D_ff_PA dff_PA12(clk, reset, regWrite,frame_offset[12],frame_offset_out[12]);
		D_ff_PA dff_PA13(clk, reset, regWrite,frame_offset[13],frame_offset_out[13]);
		D_ff_PA dff_PA14(clk, reset, regWrite,frame_offset[14],frame_offset_out[14]);
		D_ff_PA dff_PA15(clk, reset, regWrite,frame_offset[15],frame_offset_out[15]);

		D_ff_PA dff_PA16(clk, reset, regWrite,frame_offset[16],frame_offset_out[16]);
		D_ff_PA dff_PA17(clk, reset, regWrite,frame_offset[17],frame_offset_out[17]);
		D_ff_PA dff_PA18(clk, reset, regWrite,frame_offset[18],frame_offset_out[18]);
		D_ff_PA dff_PA19(clk, reset, regWrite,frame_offset[19],frame_offset_out[19]);
		
		D_ff_PA dff_PA20(clk, reset, regWrite,frame_offset[20],frame_offset_out[20]);
		D_ff_PA dff_PA21(clk, reset, regWrite,frame_offset[21],frame_offset_out[21]);
		D_ff_PA dff_PA22(clk, reset, regWrite,frame_offset[22],frame_offset_out[22]);
		D_ff_PA dff_PA23(clk, reset, regWrite,frame_offset[23],frame_offset_out[23]);

		D_ff_PA dff_PA24(clk, reset, regWrite,frame_offset[24],frame_offset_out[24]);
		D_ff_PA dff_PA25(clk, reset, regWrite,frame_offset[25],frame_offset_out[25]);
		D_ff_PA dff_PA26(clk, reset, regWrite,frame_offset[26],frame_offset_out[26]);
		D_ff_PA dff_PA27(clk, reset, regWrite,frame_offset[27],frame_offset_out[27]);
		
		D_ff_PA dff_PA28(clk, reset, regWrite,frame_offset[28],frame_offset_out[28]);
		D_ff_PA dff_PA29(clk, reset, regWrite,frame_offset[29],frame_offset_out[29]);
		D_ff_PA dff_PA30(clk, reset, regWrite,frame_offset[30],frame_offset_out[30]);
		D_ff_PA dff_PA31(clk, reset, regWrite,frame_offset[31],frame_offset_out[31]);

		D_ff_PA dff_PA32(clk, reset, regWrite,frame_offset[32],frame_offset_out[32]);
		D_ff_PA dff_PA33(clk, reset, regWrite,frame_offset[33],frame_offset_out[33]);
		D_ff_PA dff_PA34(clk, reset, regWrite,frame_offset[34],frame_offset_out[34]);
		D_ff_PA dff_PA35(clk, reset, regWrite,frame_offset[35],frame_offset_out[35]);
		
		D_ff_PA dff_PA36(clk, reset, regWrite,frame_offset[36],frame_offset_out[36]);
		D_ff_PA dff_PA37(clk, reset, regWrite,frame_offset[37],frame_offset_out[37]);
		
endmodule


module TopModule(input clk, input reset,input [41:0]VA_in,input [4:0]protection_bits,input [61:0] data_in, input CPUWriteSignal,input CPURead, input [7:0] busDataIn,output [7:0] cacheDataOut, output isValid, output L2ToWrite, output [127:0] bigblock , output exception );

//module TopModule(input clk, input reset,input [41:0]VA_in,input [4:0]protection_bits,output [29:0]page_offset_out);

wire [41:0] page_offset_out;
wire [4:0] protection_out;
wire regWrite,PA_write,hit_miss;


wire chooseOffset, FSM , writeLRU,writeReg, MRUWrite,L2Write,  way_dec_mux_sel, hit1c, hit2c;
wire [3:0] writeOffset;
wire writeValid;

//ctrlCkt cntrl(clk, reset,hit_miss,regWrite,PA_write,TLB_write);
ctrl_ckt_cache cntrlckt(clk, reset, hit_miss, hit1c, hit2c, CPUWriteSignal, CPURead,
						regWrite,	 PA_write, TLB_write ,
                      chooseOffset, FSM,  writeOffset,writeLRU,
                        writeReg, MRUWrite,  isValid, L2Write, way_dec_mux_sel, writeValid);



VA_register VA(clk,reset,regWrite,VA_in,page_offset_out);
protection_register protectionB(clk,reset,regWrite,protection_bits,protection_out);

wire not_out;
wire [4:0] destReg;
wire [31:0] decOut;
wire [61:0] out0,out1,out2,out3,out4,out5,out6,out7,out8,out9,out10,out11,out12,out13,out14,out15,out16,out17,out18,out19,out20,out21,out22,out23,out24,out25,out26,out27,out28,out29,out30,out31;
//wire [61:0] data_in;/*on miss take it from test_bench*/

not n1(not_out,hit_miss);
decoder5to32_tlb dec_tlb(destReg,decOut );
TLB_set tlb(clk,reset,not_out && TLB_write,decOut,data_in,out0,out1,out2,out3,out4,out5,out6,out7,out8,out9,out10,out11,out12,out13,out14,out15,out16,out17,out18,out19,out20,out21,out22,out23,out24,out25,out26,out27,out28,out29,out30,out31);

wire equal0,equal1,equal2,equal3,equal4,equal5,equal6,equal7,equal8,equal9,equal10,equal11,equal12,equal13,equal14,equal15,equal16,equal17,equal18,equal19,equal20,equal21,equal22,equal23,equal24,equal25,equal26,equal27,equal28,equal29,equal30,equal31;
comparator_tlb comp0(page_offset_out[41:12],out0[60:31],equal0);
comparator_tlb comp1(page_offset_out[41:12],out1[60:31],equal1);
comparator_tlb comp2(page_offset_out[41:12],out2[60:31],equal2);
comparator_tlb comp3(page_offset_out[41:12],out3[60:31],equal3);

comparator_tlb comp4(page_offset_out[41:12],out4[60:31],equal4);
comparator_tlb comp5(page_offset_out[41:12],out5[60:31],equal5);
comparator_tlb comp6(page_offset_out[41:12],out6[60:31],equal6);
comparator_tlb comp7(page_offset_out[41:12],out7[60:31],equal7);

comparator_tlb comp8(page_offset_out[41:12],out8[60:31],equal8);
comparator_tlb comp9(page_offset_out[41:12],out9[60:31],equal9);
comparator_tlb comp10(page_offset_out[41:12],out10[60:31],equal10);
comparator_tlb comp11(page_offset_out[41:12],out11[60:31],equal11);

comparator_tlb comp12(page_offset_out[41:12],out12[60:31],equal12);
comparator_tlb comp13(page_offset_out[41:12],out13[60:31],equal13);
comparator_tlb comp14(page_offset_out[41:12],out14[60:31],equal14);
comparator_tlb comp15(page_offset_out[41:12],out15[60:31],equal15);

comparator_tlb comp16(page_offset_out[41:12],out16[60:31],equal16);
comparator_tlb comp17(page_offset_out[41:12],out17[60:31],equal17);
comparator_tlb comp18(page_offset_out[41:12],out18[60:31],equal18);
comparator_tlb comp19(page_offset_out[41:12],out19[60:31],equal19);

comparator_tlb comp20(page_offset_out[41:12],out20[60:31],equal20);
comparator_tlb comp21(page_offset_out[41:12],out21[60:31],equal21);
comparator_tlb comp22(page_offset_out[41:12],out22[60:31],equal22);
comparator_tlb comp23(page_offset_out[41:12],out23[60:31],equal23);

comparator_tlb comp24(page_offset_out[41:12],out24[60:31],equal24);
comparator_tlb comp25(page_offset_out[41:12],out25[60:31],equal25);
comparator_tlb comp26(page_offset_out[41:12],out26[60:31],equal26);
comparator_tlb comp27(page_offset_out[41:12],out27[60:31],equal27);

comparator_tlb comp28(page_offset_out[41:12],out28[60:31],equal28);
comparator_tlb comp29(page_offset_out[41:12],out29[60:31],equal29);
comparator_tlb comp30(page_offset_out[41:12],out30[60:31],equal30);
comparator_tlb comp31(page_offset_out[41:12],out31[60:31],equal31);

wire [31:0]andOut;
and a0(andOut[0],out0[61],equal0);
and a1(andOut[1],out1[61],equal1);
and a2(andOut[2],out2[61],equal2);
and a3(andOut[3],out3[61],equal3);
and a4(andOut[4],out4[61],equal4);
and a5(andOut[5],out5[61],equal5);
and a6(andOut[6],out6[61],equal6);
and a7(andOut[7],out7[61],equal7);
and a8(andOut[8],out8[61],equal8);
and a9(andOut[9],out9[61],equal9);
and a10(andOut[10],out10[61],equal10);
and a11(andOut[11],out11[61],equal11);
and a12(andOut[12],out12[61],equal12);
and a13(andOut[13],out13[61],equal13);
and a14(andOut[14],out14[61],equal14);
and a15(andOut[15],out15[61],equal15);
and a16(andOut[16],out16[61],equal16);
and a17(andOut[17],out17[61],equal17);
and a18(andOut[18],out18[61],equal18);
and a19(andOut[19],out19[61],equal19);
and a20(andOut[20],out20[61],equal20);
and a21(andOut[21],out21[61],equal21);
and a22(andOut[22],out22[61],equal22);
and a23(andOut[23],out23[61],equal23);
and a24(andOut[24],out24[61],equal24);
and a25(andOut[25],out25[61],equal25);
and a26(andOut[26],out26[61],equal26);
and a27(andOut[27],out27[61],equal27);
and a28(andOut[28],out28[61],equal28);
and a29(andOut[29],out29[61],equal29);
and a30(andOut[30],out30[61],equal30);
and a31(andOut[31],out31[61],equal31);

wire [4:0] LINE;
priority_encoder_tlb prio_enc(reset,andOut,LINE);

or or1(hit_miss,andOut[0],andOut[1],andOut[2],andOut[3],andOut[4],andOut[5],andOut[6],andOut[7],andOut[8],andOut[9],andOut[10],andOut[11],andOut[12],andOut[13],andOut[14],andOut[15],andOut[16],andOut[17],andOut[18],andOut[19],andOut[20],andOut[21],andOut[22],andOut[23],andOut[24],andOut[25],andOut[26],andOut[27],andOut[28],andOut[29],andOut[30],andOut[31]);

wire [25:0] outBus;
mux_frame32to1 mux_frame(out0[30:5],out1[30:5],out2[30:5],out3[30:5],out4[30:5],out5[30:5],out6[30:5],out7[30:5],out8[30:5],out9[30:5],out10[30:5],out11[30:5],out12[30:5],out13[30:5],out14[30:5],out15[30:5],out16[30:5],out17[30:5],out18[30:5],out19[30:5],out20[30:5],out21[30:5],out22[30:5],out23[30:5],out24[30:5],out25[30:5],out26[30:5],out27[30:5],out28[30:5],out29[30:5],out30[30:5],out31[30:5],LINE,outBus );

wire [4:0] outBus_pro;
mux_protection32to1 mux_pBits(out0[4:0],out1[4:0],out2[4:0],out3[4:0],out4[4:0],out5[4:0],out6[4:0],out7[4:0],out8[4:0],out9[4:0],out10[4:0],out11[4:0],out12[4:0],out13[4:0],out14[4:0],out15[4:0],out16[4:0],out17[4:0],out18[4:0],out19[4:0],out20[4:0],out21[4:0],out22[4:0],out23[4:0],out24[4:0],out25[4:0],out26[4:0],out27[4:0],out28[4:0],out29[4:0],out30[4:0],out31[4:0],LINE, outBus_pro );

wire less_equal;
comparator_exception_tlb comp_excep(protection_out,outBus_pro,less_equal);

not not_exception(exception,less_equal);

FIFOCounter fifo(clk,not_out,reset,{out31[61],out30[61],out29[61],out28[61],out27[61],out26[61],out25[61],out24[61],out23[61],out22[61],out21[61],out20[61],out19[61],out18[61],out17[61],out16[61],out15[61],out14[61],out13[61],out12[61],out11[61],out10[61],out9[61],out8[61],out7[61],out6[61],out5[61],out4[61],out3[61],out2[61],out1[61],out0[61]},destReg);
//FIFO fifo(clk,hit_miss,destReg);

wire Write;
wire [37:0]frame_num;
and and_pa(Write,less_equal,PA_write);
register_PA reg_pa(clk,reset,Write,{outBus,page_offset_out[11:0]},frame_num);

cache_topmodule cache(clk,reset, chooseOffset, FSM, writeOffset, writeLRU,
                       writeReg, MRUWrite,   L2Write,  way_dec_mux_sel, frame_num[37:9] ,frame_num[8:4],frame_num[3:0] , CPUWriteSignal, CPURead, busDataIn, writeValid, cacheDataOut, L2ToWrite, bigblock, hit1c, hit2c);
endmodule


/*
module TB_topModule;

	// Inputs
	reg clk;
	reg reset;
	reg [41:0] VA_in;
	reg [4:0] protection_bits;
	reg [61:0] data_in;

	// Outputs
	wire [37:0] frame_num;

	// Instantiate the Unit Under Test (UUT)
	TopModule uut (
		.clk(clk), 
		.reset(reset), 
		.VA_in(VA_in), 
		.protection_bits(protection_bits), 
		.data_in(data_in), 
		.frame_num(frame_num)
	);

	always
	#5 clk=~clk;
	
	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 1;
		VA_in = 0;
		protection_bits = 0;
		data_in = 0;

		// Wait 100 ns for global reset to finish
		#10 reset=0;
        VA_in = 42'b00_0000_1111_0001_0101_0000_0100_1001_1001_0000_1110;
		  protection_bits=5'd1;
		  data_in=62'h2178A824880C0008;
	
		#20 VA_in = 42'b00_0010_1111_0001_0101_0000_0100_1001_1001_0000_1000;
		  
		  protection_bits=5'd1;
		  data_in=62'h2178A824880C0008;
		#60 $finish;

	end
      
endmodule

*/
