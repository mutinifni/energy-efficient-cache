`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:26:09 04/14/2016 
// Design Name: 
// Module Name:    lru_module 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module LRUreg(input clk, input reset,input regWrite, input [4:0] cacheindex, input hit,input g8er, input load,  
    output reg [2:0] counter );
	 
	 reg [2:0] reg3bits[31:0] ;
	 
	
	 always @(negedge clk)
	 begin
	 
	 if( reset == 1'b1)
		begin
		reg3bits[0] = 3'b000;
		reg3bits[1] = 3'b000;
		reg3bits[2] = 3'b000;
		reg3bits[3] = 3'b000;
		reg3bits[4] = 3'b000;
		reg3bits[5] = 3'b000;
		reg3bits[6] = 3'b000;
		reg3bits[7] = 3'b000;

		reg3bits[8] = 3'b000;
		reg3bits[9] = 3'b000;
		reg3bits[10] = 3'b000;
		reg3bits[11] = 3'b000;
		reg3bits[12] = 3'b000;
		reg3bits[13] = 3'b000;
		reg3bits[14] = 3'b000;
		reg3bits[15] = 3'b000;

		reg3bits[16] = 3'b000;
		reg3bits[17] = 3'b000;
		reg3bits[18] = 3'b000;
		reg3bits[19] = 3'b000;
		reg3bits[20] = 3'b000;
		reg3bits[21] = 3'b000;
		reg3bits[22] = 3'b000;
		reg3bits[23] = 3'b000;

		reg3bits[24] = 3'b000;
		reg3bits[25] = 3'b000;
		reg3bits[26] = 3'b000;
		reg3bits[27] = 3'b000;
		reg3bits[28] = 3'b000;
		reg3bits[29] = 3'b000;
		reg3bits[30] = 3'b000;
		reg3bits[31] = 3'b000;

		end
	 
	 if( load == 1'b1 && hit==1 && regWrite == 1'b1 )
		reg3bits[cacheindex] = 3'b111;
	 if( regWrite == 1'b1 && g8er == 1'b1 && hit==1)
		begin
		 reg3bits[cacheindex] = reg3bits[cacheindex] - 1;
	 	end 
	 counter[2:0] = reg3bits[cacheindex];	 
	end

endmodule
module demux3to8(
	input [2:0] lineindex,
   output reg [7:0] selway );
	
	always @ ( lineindex )
	begin
		case( lineindex )
			3'b000: selway = 8'b00000001;
			3'b001: selway = 8'b00000010;
			3'b010: selway = 8'b00000100;
			3'b011: selway = 8'b00001000;
			3'b100: selway = 8'b00010000;
			3'b101: selway = 8'b00100000;
			3'b110: selway = 8'b01000000;
			3'b111: selway = 8'b10000000;
		endcase	
	end
	 
endmodule

module mux24to3(
	input [23:0] wayIn,
	input [2:0] selWay, 
	output reg [2:0] wayOut
	);
	
	always @(*)
	begin
	
	  case( selWay )
	
		3'b000: wayOut = wayIn[2:0];
		3'b001: wayOut = wayIn[5:3];
		3'b010: wayOut = wayIn[8:6];
		3'b011: wayOut = wayIn[11:9];
		3'b100: wayOut = wayIn[14:12];
		3'b101: wayOut = wayIn[17:15];
		3'b110: wayOut = wayIn[20:18];
		3'b111: wayOut = wayIn[23:21];
		
	  endcase
	end
endmodule

module mux2to1(
	input [2:0] zeroWay,
	input [2:0] wayIn,
	input  selOp,
	output reg [2:0] wayOut
	);
	
	always @(*)
	begin
		
		if( selOp == 1'b1 )
			wayOut = wayIn;
		else
			wayOut = zeroWay;
	
	end
endmodule	


module comparator(
	input [2:0] wayData,
	input [2:0] selWay,input hit,
	output  reg opOut
	);
	
	always @(wayData or selWay or hit)
	begin
		if ( wayData > selWay )
			opOut = 1'b1;
		 else
			opOut = 1'b0;	
	end

endmodule
module EqualComp(input [2:0] in0,input [2:0] in1,output reg out);

	always@(in0,in1)
		begin
			if(in0 == in1) 
				out=1'b1;
			else
				out=1'b0;
		end
endmodule

module encoderVI(input [7:0] in, output reg [2:0] out);

	always @(in)
	begin
	
		if( in[0] == 1'b0)
			out = 3'b000;
		else if ( in[1] == 1'b0)
			out = 3'b001;
		else if ( in[2] == 1'b0)
			out = 3'b010;
		else if ( in[3] == 1'b0)
			out = 3'b011;
		else if ( in[4] == 1'b0)
			out = 3'b100;
		else if ( in[5] == 1'b0)
			out = 3'b101;
		else if ( in[6] == 1'b0)
			out = 3'b110;
		else if ( in[7] == 1'b0)
			out = 3'b111;
		
		end
endmodule

module muxVI(input [2:0] inVI, input [2:0] inPrio,input sel ,output reg [2:0] out);

	always @(*)
	begin
		if( sel == 1'b0)
				out = inVI;
		else
			out = inPrio;
	end
endmodule

module PEncoder8to3(input  reset,input  [7:0] in,output reg [2:0] out);

	always@(reset,in)
		begin
		if(reset==1'b1)				out=3'b000;
		else if(in[0]==1'b1)			out=3'b000;
		else if(in[1]==1'b1)			out=3'b001;
		else if(in[2]==1'b1)			out=3'b010;
		else if(in[3]==1'b1)			out=3'b011;
		else if(in[4]==1'b1)			out=3'b100;
		else if(in[5]==1'b1)			out=3'b101;
		else if(in[6]==1'b1)			out=3'b110;
		else if(in[7]==1'b1)			out=3'b111;
		end

endmodule

module LRUCounter(
		input clk,
		input	[2:0] hitWay,
		input	[4:0] cacheIndex,
		input hit,
		input reset,
		input regWrite,
		input [7:0] vi,
		output reg [2:0] selWayOut
);

	wire [2:0] selWay,out_lru;
	wire [2:0] selectWay3bits;
	wire [7:0] selectWay8bits;
	wire [2:0] ctrWay0,ctrWay1,ctrWay2,ctrWay3,ctrWay4,ctrWay5,ctrWay6,ctrWay7;
	wire [2:0] selectCtrOut;
	wire Opgtr0,Opgtr1,Opgtr2,Opgtr3,Opgtr4,Opgtr5,Opgtr6,Opgtr7;
	
	wire viAnd;
	wire [2:0] wayVI;
	and andVI( viAnd, vi[0], vi[1], vi[2], vi[3], vi[4], vi[5], vi[6], vi[7]);
	encoderVI prio_VIEncoder( vi, wayVI);
	
	muxVI muxSelVI( wayVI , out_lru, viAnd, selWay);
	
	mux2to1 muxWaySelect( selWay, hitWay, hit, selectWay3bits);

	demux3to8 demux3to8SelectWay ( selectWay3bits, selectWay8bits );

	LRUreg reg_way0(clk, reset, regWrite, cacheIndex, hit,Opgtr0, selectWay8bits[0],ctrWay0);
	LRUreg reg_way1(clk, reset, regWrite, cacheIndex, hit,Opgtr1, selectWay8bits[1],ctrWay1);
	LRUreg reg_way2(clk, reset, regWrite, cacheIndex, hit, Opgtr2,selectWay8bits[2],ctrWay2);
	LRUreg reg_way3(clk, reset, regWrite, cacheIndex,  hit,Opgtr3,selectWay8bits[3],ctrWay3);
	LRUreg reg_way4(clk, reset, regWrite, cacheIndex,  hit,Opgtr4,selectWay8bits[4],ctrWay4);
	LRUreg reg_way5(clk, reset, regWrite, cacheIndex,  hit,Opgtr5,selectWay8bits[5],ctrWay5);
	LRUreg reg_way6(clk, reset, regWrite, cacheIndex,  hit,Opgtr6,selectWay8bits[6],ctrWay6);
	LRUreg reg_way7(clk, reset, regWrite, cacheIndex,  hit,Opgtr7,selectWay8bits[7],ctrWay7);

	mux24to3 muxSelectGreater(
	  { ctrWay7,ctrWay6,ctrWay5,ctrWay4,ctrWay3,ctrWay2,ctrWay1,ctrWay0},
		 selectWay3bits,selectCtrOut);
		 
	comparator cmpway0( ctrWay0, selectCtrOut,hit, Opgtr0);	 
   comparator cmpway1( ctrWay1, selectCtrOut,hit, Opgtr1);
	comparator cmpway2( ctrWay2, selectCtrOut,hit, Opgtr2);
	comparator cmpway3( ctrWay3, selectCtrOut,hit, Opgtr3);
	comparator cmpway4( ctrWay4, selectCtrOut,hit, Opgtr4);
	comparator cmpway5( ctrWay5, selectCtrOut,hit, Opgtr5);
	comparator cmpway6( ctrWay6, selectCtrOut,hit, Opgtr6);
	comparator cmpway7( ctrWay7, selectCtrOut,hit, Opgtr7);
	
	wire [7:0] out_eq;
	EqualComp eq0(ctrWay0,3'b000,out_eq[0]);
	EqualComp eq1(ctrWay1,3'b000,out_eq[1]);
	EqualComp eq2(ctrWay2,3'b000,out_eq[2]);
	EqualComp eq3(ctrWay3,3'b000,out_eq[3]);
	EqualComp eq4(ctrWay4,3'b000,out_eq[4]);
	EqualComp eq5(ctrWay5,3'b000,out_eq[5]);
	EqualComp eq6(ctrWay6,3'b000,out_eq[6]);
	EqualComp eq7(ctrWay7,3'b000,out_eq[7]);
	
	PEncoder8to3 encoder(reset,out_eq,out_lru);
	always @(*)
		selWayOut = selWay;
  
endmodule

module testBench();

reg clk;
reg [2:0] hitWay;
reg [4:0] cacheIndex;
reg hit;
reg regWrite;
reg [7:0] vi;
wire [2:0] selWay;
reg reset;

	LRUCounter lru(clk,hitWay,cacheIndex,hit, reset, regWrite, vi,selWay);
	
	
	
	always begin
		#5 clk=~clk;
	end
	
		
	initial
	begin
	clk = 0;
	//cacheIndex = 5'b00000;
	hitWay = 5'b000;
	reset = 1'b1; cacheIndex = 5'b00000;
	regWrite = 1'b1;
	vi = 8'b11111111;
		//hitWay = 3'bz;
		
		//hit = 1'bz;
		
		//#5 cacheIndex = 5'b00000;
		/*
		reset = 1'b1;
		#5 cacheIndex = 5'b00001;
		#5 cacheIndex = 5'b00010;
		#5 cacheIndex = 5'b00011;
		#5 cacheIndex = 5'b00100;
		#5 cacheIndex = 5'b00101;
		#5 cacheIndex = 5'b00110;
		#5 cacheIndex = 5'b00111;
		*/
		//#10 reset = 1'b0;
		 
	//	#10 cacheIndex = 5'b00000;
		
		#10 reset = 1'b0;  hitWay = 3'b000;cacheIndex =5'b00000; hit = 1'b1;
		#20 hitWay = 3'b001;cacheIndex =5'b00000; hit = 1'b0;
		
		#20 hitWay = 3'b001;cacheIndex =5'b00000; hit = 1'b1;
		
		#20 hitWay = 3'b000;cacheIndex =5'b00000; hit = 1'b0;

		#20 hitWay = 3'b010;cacheIndex =5'b00000; hit = 1'b1;
		
		
		
		#20 hitWay = 3'b010;cacheIndex =5'b00000; hit = 1'b0;

		#20 hitWay = 3'b011;cacheIndex =5'b00000; hit = 1'b1;
		#20 hitWay = 3'b011;cacheIndex =5'b00000; hit = 1'b0;

		#20 hitWay = 3'b001;cacheIndex =5'b00000; hit = 1'b1;
		#20 hitWay = 3'b001;cacheIndex =5'b00000; hit = 1'b0;
		
		/*#10 reset = 1'b1; cacheIndex = 5'b00000;
		
		#10 reset = 1'b1; cacheIndex = 5'b00001;
		#10 reset = 1'b0; cacheIndex = 5'b00001;
		
		#10 hitWay = 3'b000;cacheIndex =5'b00001; hit = 1'b1;
		#10 hitWay = 3'b000;cacheIndex =5'b00001; hit = 1'b0;*/

		#10$finish;
		
	end
	
	
	
endmodule

module tb_reg;

	// Inputs
	reg clk;
	reg reset;
	reg regWrite;
	reg [4:0] cacheindex;
	reg dcr;
	reg load;
	reg [2:0] data;

	// Outputs
	wire [2:0] counter;

	// Instantiate the Unit Under Test (UUT)
	LRUreg uut (
		.clk(clk), 
		.reset(reset), 
		.regWrite(regWrite), 
		.cacheindex(cacheindex), 
		.dcr(dcr), 
		.load(load), 
		.data(data), 
		.counter(counter)
	);
always begin
		#5 clk=~clk;
	end 
	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 1;
		regWrite = 1;
		cacheindex = 0;
		dcr = 0;
		load = 0;
		data = 3'b111;

		// Wait 100 ns for global reset to finish
		#10 reset=0;
		cacheindex = 5'd0;
		dcr = 0;
		load = 1;
		#10 
		cacheindex = 5'd0;
		dcr = 0;
		load = 1;
		#10 
		cacheindex = 5'd0;
		dcr = 1;
		load = 0;
		#10 
		cacheindex = 5'd0;
		dcr = 0;
		load = 1;
		#10 
		cacheindex = 5'd0;
		dcr = 1;
		load = 0;
        #10 $finish;
		// Add stimulus here

	end
      
endmodule


