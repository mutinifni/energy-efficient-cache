`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:47:25 04/08/2016 
// Design Name: 
// Module Name:    FIFOCounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module FIFOreg(input clk, input reset, input dcr, input load, input [4:0] data,output reg [4:0] counter );
	 
	 reg [4:0] reg3bits;
	 
		 
	 always @(posedge clk)
	 begin
	 
	 if( reset == 1'b1)
		 reg3bits = 5'b00000;
	 if( load == 1'b1 )
		reg3bits = data;
	 if( dcr == 1'b1 )
		begin
		 if ( reg3bits > 3'b000 )
		 reg3bits = reg3bits - 1;
	 	end 
	 counter[4:0] = reg3bits;	 
	end

endmodule

module demux5to32(
	input [4:0] lineindex,
   output reg [31:0] selway );
	
	always @ ( lineindex )
	begin
		case( lineindex )
			5'b00000: selway = 31'd1;
			5'b00001: selway = 31'd1<<1;
			5'b00010: selway = 31'd1<<2;
			5'b00011: selway = 31'd1<<3;
			5'b00100: selway = 31'd1<<4;
			5'b00101: selway = 31'd1<<5;
			5'b00110: selway = 31'd1<<6;
			5'b00111: selway = 31'd1<<7;
			5'b01000: selway = 31'd1<<8;
			5'b01001: selway = 31'd1<<9;
			5'b01010: selway = 31'd1<<10;
			5'b01011: selway = 31'd1<<11;
			5'b01100: selway = 31'd1<<12;
			5'b01101: selway = 31'd1<<13;
			5'b01110: selway = 31'd1<<14;
			5'b01111: selway = 31'd1<<15;
			5'b10000: selway = 31'd1<<16;
			5'b10001: selway = 31'd1<<17;
			5'b10010: selway = 31'd1<<18;
			5'b10011: selway = 31'd1<<19;
			5'b10100: selway = 31'd1<<20;
			5'b10101: selway = 31'd1<<21;
			5'b10110: selway = 31'd1<<22;
			5'b10111: selway = 31'd1<<23;
			5'b11000: selway = 31'd1<<24;
			5'b11001: selway = 31'd1<<25;
			5'b11010: selway = 31'd1<<26;
			5'b11011: selway = 31'd1<<27;
			5'b11100: selway = 31'd1<<28;
			5'b11101: selway = 31'd1<<29;
			5'b11110: selway = 31'd1<<30;
			5'b11111: selway = 31'd1<<31;
			
			
			endcase	
	end
	 
endmodule


module demux5to160( 
	input [4:0] lineindex, 
	output reg [159:0] selway
	);	 

	always @( * )
	begin
	
		case( lineindex )
		
			5'b00000: selway = 160'd31;
			5'b00001: selway = 160'd31<<5;
			5'b00010: selway = 160'd31<<10;
			5'b00011: selway = 160'd31<<15;
			5'b00100: selway = 160'd31<<20;
			5'b00101: selway = 160'd31<<25;
			5'b00110: selway = 160'd31<<30;
			5'b00111: selway = 160'd31<<35;
			5'b01000: selway = 160'd31<<40;
			5'b01001: selway = 160'd31<<45;
			5'b01010: selway = 160'd31<<50;
			5'b01011: selway = 160'd31<<55;
			5'b01100: selway = 160'd31<<60;
			5'b01101: selway = 160'd31<<65;
			5'b01110: selway = 160'd31<<70;
			5'b01111: selway = 160'd31<<75;
			5'b10000: selway = 160'd31<<80;
			5'b10001: selway = 160'd31<<85;
			5'b10010: selway = 160'd31<<90;
			5'b10011: selway = 160'd31<<95;
			5'b10100: selway = 160'd31<<100;
			5'b10101: selway = 160'd31<<105;
			5'b10110: selway = 160'd31<<110;
			5'b10111: selway = 160'd31<<115;
			5'b11000: selway = 160'd31<<120;
			5'b11001: selway = 160'd31<<125;
			5'b11010: selway = 160'd31<<130;
			5'b11011: selway = 160'd31<<135;
			5'b11100: selway = 160'd31<<140;
			5'b11101: selway = 160'd31<<145;
			5'b11110: selway = 160'd31<<150;
			5'b11111: selway = 160'd31<<155;
			
			
	endcase    
   end


endmodule

module mux32to1(
	input [159:0] wayIn,
	input [4:0] selWay, 
	output reg [4:0] wayOut
	);
	
	always @(*)
	begin
	
	  case( selWay )
	
		5'b00000: wayOut = wayIn[4:0];
		5'b00001: wayOut = wayIn[9:5];
		5'b00010: wayOut = wayIn[14:10];
		5'b00011: wayOut = wayIn[19:15];
		5'b00100: wayOut = wayIn[24:20];
		5'b00101: wayOut = wayIn[29:25];
		5'b00110: wayOut = wayIn[34:30];
		5'b00111: wayOut = wayIn[39:35];
		5'b01000: wayOut = wayIn[44:40];
		5'b01001: wayOut = wayIn[49:45];
		5'b01010: wayOut = wayIn[54:50];
		5'b01011: wayOut = wayIn[59:55];
		5'b01100: wayOut = wayIn[64:60];
		5'b01101: wayOut = wayIn[69:65];
		5'b01110: wayOut = wayIn[74:70];
		5'b01111: wayOut = wayIn[79:75];


		5'b10000: wayOut = wayIn[84:80];
		5'b10001: wayOut = wayIn[89:85];
		5'b10010: wayOut = wayIn[94:90];
		5'b10011: wayOut = wayIn[99:95];
		5'b10100: wayOut = wayIn[104:100];
		5'b10101: wayOut = wayIn[109:105];
		5'b10110: wayOut = wayIn[114:110];
		5'b10111: wayOut = wayIn[119:115];
		5'b11000: wayOut = wayIn[124:120];
		5'b11001: wayOut = wayIn[129:125];
		5'b11010: wayOut = wayIn[134:130];
		5'b11011: wayOut = wayIn[139:135];
		5'b11100: wayOut = wayIn[144:140];
		5'b11101: wayOut = wayIn[149:145];
		5'b11110: wayOut = wayIn[154:150];
		5'b11111: wayOut = wayIn[159:155];
		
		
	  endcase
	end
endmodule

module mux2to1FIFO(
	input [4:0] zeroWay,
	input [4:0] wayIn,
	input  selOp,
	output reg [4:0] wayOut
	);
	
	always @(*)
	begin
		
		if( selOp == 1'b1 )
			wayOut = wayIn;
		else
			wayOut = zeroWay;
	
	end
	
endmodule	

module prioEncoderFIFO(
	input [4:0] way1,
	input [4:0] way2,
	input [4:0] way3,
	input [4:0] way4,
	input [4:0] way5,
	input [4:0] way6,
	input [4:0] way7,
	input [4:0] way8,
	input [4:0] way9,
	input [4:0] way10,
	input [4:0] way11,
	input [4:0] way12,
	input [4:0] way13,
	input [4:0] way14,
	input [4:0] way15,
	input [4:0] way16,
	input [4:0] way17,
	input [4:0] way18,
	input [4:0] way19,
	input [4:0] way20,
	input [4:0] way21,
	input [4:0] way22,
	input [4:0] way23,
	input [4:0] way24,
	input [4:0] way25,
	input [4:0] way26,
	input [4:0] way27,
	input [4:0] way28,
	input [4:0] way29,
	input [4:0] way30,
	input [4:0] way31,
	input [4:0] way32,
	output reg [4:0] selWay
	);
	
	always @(*)
	begin
		if ( way1 == 5'b0000 )
				selWay = 5'b00000;
		else if ( way2 == 5'b0000 )
				selWay = 5'b00001;
		else if ( way3 == 5'b00000 )
				selWay = 5'b00010;
		else if ( way4 == 5'b00000 )
				selWay = 5'b00011;
		else if ( way5 == 5'b00000 )
				selWay = 5'b00100;
		else if ( way6 == 5'b00000 )
				selWay = 5'b00101;
		else if ( way7 == 5'b00000 )
				selWay = 5'b00110;
		else if ( way8 == 5'b00000 )
				selWay = 5'b00111;		
		
      else if ( way9 == 5'b0000 )
				selWay = 5'b01000;
		else if ( way10 == 5'b0000 )
				selWay = 5'b01001;
		else if ( way11 == 5'b00000 )
				selWay = 5'b01010;
		else if ( way12 == 5'b00000 )
				selWay = 5'b01011;
		else if ( way13 == 5'b00000 )
				selWay = 5'b01100;
		else if ( way14 == 5'b00000 )
				selWay = 5'b01101;
		else if ( way15 == 5'b00000 )
				selWay = 5'b01110;
		else if ( way16 == 5'b00000 )
				selWay = 5'b01111;				
				
	
		else if ( way17 == 5'b0000 )
				selWay = 5'b10000;
		else if ( way18 == 5'b0000 )
				selWay = 5'b10001;
		else if ( way19 == 5'b00000 )
				selWay = 5'b10010;
		else if ( way20 == 5'b00000 )
				selWay = 5'b10011;
		else if ( way21 == 5'b00000 )
				selWay = 5'b10100;
		else if ( way22 == 5'b00000 )
				selWay = 5'b10101;
		else if ( way23 == 5'b00000 )
				selWay = 5'b10110;
		else if ( way24 == 5'b00000 )
				selWay = 5'b10111;		
		
      else if ( way25 == 5'b0000 )
				selWay = 5'b11000;
		else if ( way26 == 5'b0000 )
				selWay = 5'b11001;
		else if ( way27 == 5'b00000 )
				selWay = 5'b11010;
		else if ( way28 == 5'b00000 )
				selWay = 5'b11011;
		else if ( way29 == 5'b00000 )
				selWay = 5'b11100;
		else if ( way30 == 5'b00000 )
				selWay = 5'b11101;
		else if ( way31 == 5'b00000 )
				selWay = 5'b11110;
		else if ( way32 == 5'b00000 )
				selWay = 5'b11111;					
	end

endmodule

module comparatorFIFO(
	input [4:0] wayData,
	input [4:0] selWay,
	output  reg opOut
	);
	
	always @(*)
	begin
		if ( wayData >= selWay )
			opOut = 1'b1;
		 else
			opOut = 1'b0;	
	end

endmodule

module muxVIfifo( input [4:0] in1, input [4:0] in2, input vi, output reg [4:0] out);
	
		always @( * )
		begin
			if( vi == 1'b0)
				out = in1;
			else
				out = in2;	
		end
endmodule

module encoderVIfifo(
	input [31:0] in,
	output reg [4:0] out
	
	);
		
		always @(*)
		begin
		
		if( in[0] == 1'b0)
			out = 5'b00000;
		else	if( in[1] == 1'b0)
			out = 5'b00001;
		else	if( in[2] == 1'b0)
			out = 5'b00010;
		else	if( in[3] == 1'b0)
			out = 5'b00011;
		else	if( in[4] == 1'b0)
			out = 5'b00100;
		else	if( in[5] == 1'b0)
			out = 5'b00101;
		else	if( in[6] == 1'b0)
			out = 5'b00110;
		else	if( in[7] == 1'b0)
			out = 5'b00111;
		else	if( in[8] == 1'b0)
			out = 5'b01000;
		else	if( in[9] == 1'b0)
			out = 5'b01001;
		else	if( in[10] == 1'b0)
			out = 5'b01010;
		else	if( in[11] == 1'b0)
			out = 5'b01011;
		else	if( in[12] == 1'b0)
			out = 5'b01100;
		else	if( in[13] == 1'b0)
			out = 5'b01101;
		else	if( in[14] == 1'b0)
			out = 5'b01110;
		else	if( in[15] == 1'b0)
			out = 5'b01111;
		
		else	if( in[16] == 1'b0)
			out = 5'b10000;
		else	if( in[17] == 1'b0)
			out = 5'b10001;
		else	if( in[18] == 1'b0)
			out = 5'b10010;
		else	if( in[19] == 1'b0)
			out = 5'b10011;
		else	if( in[20] == 1'b0)
			out = 5'b10100;
		else	if( in[21] == 1'b0)
			out = 5'b10101;
		else	if( in[22] == 1'b0)
			out = 5'b10110;
		else	if( in[23] == 1'b0)
			out = 5'b10111;
		else	if( in[24] == 1'b0)
			out = 5'b11000;
		else	if( in[25] == 1'b0)
			out = 5'b11001;
		else	if( in[26] == 1'b0)
			out = 5'b11010;
		else	if( in[27] == 1'b0)
			out = 5'b11011;
		else	if( in[28] == 1'b0)
			out = 5'b11100;
		else	if( in[29] == 1'b0)
			out = 5'b11101;
		else	if( in[30] == 1'b0)
				out = 5'b11110;
		else	if( in[31] == 1'b0)
			out = 5'b11111;
		end	
endmodule

module FIFOCounter(
		input clk,
		input hit,
		input reset,
		input [31:0] vi,
		output reg [4:0] selWay
);

	wire [4:0] zeroWay;
	wire [4:0] selectWay5bits;
	wire [31:0] selectWay32bits;
	wire [159:0] selectWay160bits;
	//wire ctrDcr0,ctrDcr1,ctrDcr2,ctrDcr3,ctrDcr4,ctrDcr5,ctrDcr6,ctrDcr7;
	wire ctrDcr[31:0];
	//wire [2:0] ctrWay0,ctrWay1,ctrWay2,ctrWay3,ctrWay4,ctrWay5,ctrWay6,ctrWay7;
	wire [4:0] ctrWay[31:0];
	wire [4:0] selectCtrOut;
	//wire Opgtr0,Opgtr1,Opgtr2,Opgtr3,Opgtr4,Opgtr5,Opgtr6,Opgtr7;
	wire Opgtr[31:0];
	//wire OpHit0, OpHit1, OpHit2, OpHit3, OpHit4, OpHit5, OpHit6, OpHit7;
	wire OpHit[31:0];
	wire [4:0] viWay;
	wire viAnd;
	
	and andVI( viAnd, vi[0],vi[1],vi[2], vi[3],vi[4],vi[5],vi[6],vi[7],vi[8],vi[9],vi[10],vi[11],vi[12],vi[13],vi[14],vi[15],vi[16],vi[17],vi[18],vi[19],vi[20],vi[21],vi[22],vi[23],vi[24],vi[25],vi[26],vi[27],vi[28],vi[29],vi[30],vi[31]);
	//and andVI( viAnd, vi[31:0]);
	
	encoderVIfifo prio_vi( vi, viWay);
	muxVIfifo muxWaySelect( viWay, zeroWay, viAnd, selectWay5bits);

	demux5to32 demux3to8SelectWay ( selectWay5bits, selectWay32bits );
	demux5to160 demux3to24Selectway( selectWay5bits, selectWay160bits);

	FIFOreg reg_way0(clk, reset, ctrDcr[0], selectWay32bits[0], selectWay160bits[4:0],ctrWay[0]);
	FIFOreg reg_way1(clk, reset, ctrDcr[1], selectWay32bits[1], selectWay160bits[9:5],ctrWay[1]);
	FIFOreg reg_way2(clk, reset, ctrDcr[2], selectWay32bits[2], selectWay160bits[14:10],ctrWay[2]);
	FIFOreg reg_way3(clk, reset, ctrDcr[3], selectWay32bits[3], selectWay160bits[19:15],ctrWay[3]);
	FIFOreg reg_way4(clk, reset, ctrDcr[4], selectWay32bits[4], selectWay160bits[24:20],ctrWay[4]);
	FIFOreg reg_way5(clk, reset, ctrDcr[5], selectWay32bits[5], selectWay160bits[29:25],ctrWay[5]);
	FIFOreg reg_way6(clk, reset, ctrDcr[6], selectWay32bits[6], selectWay160bits[34:30],ctrWay[6]);
	FIFOreg reg_way7(clk, reset, ctrDcr[7], selectWay32bits[7], selectWay160bits[39:35],ctrWay[7]);
   
	FIFOreg reg_way8(clk, reset, ctrDcr[8], selectWay32bits[8], selectWay160bits[44:40],ctrWay[8]);
	FIFOreg reg_way9(clk, reset, ctrDcr[9], selectWay32bits[9], selectWay160bits[49:45],ctrWay[9]);
	FIFOreg reg_way10(clk, reset, ctrDcr[10], selectWay32bits[10], selectWay160bits[54:50],ctrWay[10]);
	FIFOreg reg_way11(clk, reset, ctrDcr[11], selectWay32bits[11], selectWay160bits[59:55],ctrWay[11]);
	FIFOreg reg_way12(clk, reset, ctrDcr[12], selectWay32bits[12], selectWay160bits[64:60],ctrWay[12]);
	FIFOreg reg_way13(clk, reset, ctrDcr[13], selectWay32bits[13], selectWay160bits[69:65],ctrWay[13]);
	FIFOreg reg_way14(clk, reset, ctrDcr[14], selectWay32bits[14], selectWay160bits[74:70],ctrWay[14]);
	FIFOreg reg_way15(clk, reset, ctrDcr[15], selectWay32bits[15], selectWay160bits[79:75],ctrWay[15]);

   FIFOreg reg_way16(clk, reset, ctrDcr[16], selectWay32bits[16], selectWay160bits[84:80],ctrWay[16]);
	FIFOreg reg_way17(clk, reset, ctrDcr[17], selectWay32bits[17], selectWay160bits[89:85],ctrWay[17]);
	FIFOreg reg_way18(clk, reset, ctrDcr[18], selectWay32bits[18], selectWay160bits[94:90],ctrWay[18]);
	FIFOreg reg_way19(clk, reset, ctrDcr[19], selectWay32bits[19], selectWay160bits[99:95],ctrWay[19]);
	FIFOreg reg_way20(clk, reset, ctrDcr[20], selectWay32bits[20], selectWay160bits[104:100],ctrWay[20]);
	FIFOreg reg_way21(clk, reset, ctrDcr[21], selectWay32bits[21], selectWay160bits[109:105],ctrWay[21]);
	FIFOreg reg_way22(clk, reset, ctrDcr[22], selectWay32bits[22], selectWay160bits[114:110],ctrWay[22]);
	FIFOreg reg_way23(clk, reset, ctrDcr[23], selectWay32bits[23], selectWay160bits[119:115],ctrWay[23]);
   
	FIFOreg reg_way24(clk, reset, ctrDcr[24], selectWay32bits[24], selectWay160bits[124:120],ctrWay[24]);
	FIFOreg reg_way25(clk, reset, ctrDcr[25], selectWay32bits[25], selectWay160bits[129:125],ctrWay[25]);
	FIFOreg reg_way26(clk, reset, ctrDcr[26], selectWay32bits[26], selectWay160bits[134:130],ctrWay[26]);
	FIFOreg reg_way27(clk, reset, ctrDcr[27], selectWay32bits[27], selectWay160bits[139:135],ctrWay[27]);
	FIFOreg reg_way28(clk, reset, ctrDcr[28], selectWay32bits[28], selectWay160bits[144:140],ctrWay[28]);
	FIFOreg reg_way29(clk, reset, ctrDcr[29], selectWay32bits[29], selectWay160bits[149:145],ctrWay[29]);
	FIFOreg reg_way30(clk, reset, ctrDcr[30], selectWay32bits[30], selectWay160bits[154:150],ctrWay[30]);
	FIFOreg reg_way31(clk, reset, ctrDcr[31], selectWay32bits[31], selectWay160bits[159:155],ctrWay[31]);

	mux32to1 muxSelectGreater(
	  { ctrWay[31],ctrWay[30],ctrWay[29],ctrWay[28],ctrWay[27],ctrWay[26],ctrWay[25],ctrWay[24],ctrWay[23],ctrWay[22],ctrWay[21],ctrWay[20],ctrWay[19],ctrWay[18],ctrWay[17],ctrWay[16],ctrWay[15],ctrWay[14],ctrWay[13],ctrWay[12],ctrWay[11],ctrWay[10],ctrWay[9],ctrWay[8],ctrWay[7],ctrWay[6],ctrWay[5],ctrWay[4],ctrWay[3],ctrWay[2],ctrWay[1],ctrWay[0]},
		 selectWay5bits,selectCtrOut);
		 
	comparatorFIFO cmpway0( ctrWay[0], selectCtrOut, Opgtr[0]);	 
   comparatorFIFO cmpway1( ctrWay[1], selectCtrOut, Opgtr[1]);
	comparatorFIFO cmpway2( ctrWay[2], selectCtrOut, Opgtr[2]);
	comparatorFIFO cmpway3( ctrWay[3], selectCtrOut, Opgtr[3]);
	comparatorFIFO cmpway4( ctrWay[4], selectCtrOut, Opgtr[4]);
	comparatorFIFO cmpway5( ctrWay[5], selectCtrOut, Opgtr[5]);
	comparatorFIFO cmpway6( ctrWay[6], selectCtrOut, Opgtr[6]);
	comparatorFIFO cmpway7( ctrWay[7], selectCtrOut, Opgtr[7]);

	comparatorFIFO cmpway8( ctrWay[8], selectCtrOut, Opgtr[8]);	 
   comparatorFIFO cmpway9( ctrWay[9], selectCtrOut, Opgtr[9]);
	comparatorFIFO cmpway10( ctrWay[10], selectCtrOut, Opgtr[10]);
	comparatorFIFO cmpway11( ctrWay[11], selectCtrOut, Opgtr[11]);
	comparatorFIFO cmpway12( ctrWay[12], selectCtrOut, Opgtr[12]);
	comparatorFIFO cmpway13( ctrWay[13], selectCtrOut, Opgtr[13]);
	comparatorFIFO cmpway14( ctrWay[14], selectCtrOut, Opgtr[14]);
	comparatorFIFO cmpway15( ctrWay[15], selectCtrOut, Opgtr[15]);

	comparatorFIFO cmpway16( ctrWay[16], selectCtrOut, Opgtr[16]);	 
   comparatorFIFO cmpway17( ctrWay[17], selectCtrOut, Opgtr[17]);
	comparatorFIFO cmpway18( ctrWay[18], selectCtrOut, Opgtr[18]);
	comparatorFIFO cmpway19( ctrWay[19], selectCtrOut, Opgtr[19]);
	comparatorFIFO cmpway20( ctrWay[20], selectCtrOut, Opgtr[20]);
	comparatorFIFO cmpway21( ctrWay[21], selectCtrOut, Opgtr[21]);
	comparatorFIFO cmpway22( ctrWay[22], selectCtrOut, Opgtr[22]);
	comparatorFIFO cmpway23( ctrWay[23], selectCtrOut, Opgtr[23]);

	comparatorFIFO cmpway24( ctrWay[24], selectCtrOut, Opgtr[24]);	 
   comparatorFIFO cmpway25( ctrWay[25], selectCtrOut, Opgtr[25]);
	comparatorFIFO cmpway26( ctrWay[26], selectCtrOut, Opgtr[26]);
	comparatorFIFO cmpway27( ctrWay[27], selectCtrOut, Opgtr[27]);
	comparatorFIFO cmpway28( ctrWay[28], selectCtrOut, Opgtr[28]);
	comparatorFIFO cmpway29( ctrWay[29], selectCtrOut, Opgtr[29]);
	comparatorFIFO cmpway30( ctrWay[30], selectCtrOut, Opgtr[30]);
	comparatorFIFO cmpway31( ctrWay[31], selectCtrOut, Opgtr[31]);

	
	and andHit0( OpHit[0], hit , Opgtr[0]);
	and andHit1( OpHit[1], hit , Opgtr[1]);
	and andHit2( OpHit[2], hit , Opgtr[2]);
	and andHit3( OpHit[3], hit , Opgtr[3]);
	and andHit4( OpHit[4], hit , Opgtr[4]);
	and andHit5( OpHit[5], hit , Opgtr[5]);
	and andHit6( OpHit[6], hit , Opgtr[6]);
	and andHit7( OpHit[7], hit , Opgtr[7]);
	
	and andHit8( OpHit[8], hit , Opgtr[8]);
	and andHit9( OpHit[9], hit , Opgtr[9]);
	and andHit10( OpHit[10], hit , Opgtr[10]);
	and andHit11( OpHit[11], hit , Opgtr[11]);
	and andHit12( OpHit[12], hit , Opgtr[12]);
	and andHit13( OpHit[13], hit , Opgtr[13]);
	and andHit14( OpHit[14], hit , Opgtr[14]);
	and andHit15( OpHit[15], hit , Opgtr[15]);
	
	and andHit16( OpHit[16], hit , Opgtr[16]);
	and andHit17( OpHit[17], hit , Opgtr[17]);
	and andHit18( OpHit[18], hit , Opgtr[18]);
	and andHit19( OpHit[19], hit , Opgtr[19]);
	and andHit20( OpHit[20], hit , Opgtr[20]);
	and andHit21( OpHit[21], hit , Opgtr[21]);
	and andHit22( OpHit[22], hit , Opgtr[22]);
	and andHit23( OpHit[23], hit , Opgtr[23]);
	
	and andHit24( OpHit[24], hit , Opgtr[24]);
	and andHit25( OpHit[25], hit , Opgtr[25]);
	and andHit26( OpHit[26], hit , Opgtr[26]);
	and andHit27( OpHit[27], hit , Opgtr[27]);
	and andHit28( OpHit[28], hit , Opgtr[28]);
	and andHit29( OpHit[29], hit , Opgtr[29]);
	and andHit30( OpHit[30], hit , Opgtr[30]);
	and andHit31( OpHit[31], hit , Opgtr[31]);
	
	//and andHit[31:0](OpHit[31:0],hit, Opgtr[31:0]);
	
	and andWay0( ctrDcr[0], OpHit[0], ~selectWay32bits[0] );
	and andWay1( ctrDcr[1], OpHit[1], ~selectWay32bits[1] );
	and andWay2( ctrDcr[2], OpHit[2], ~selectWay32bits[2] );
	and andWay3( ctrDcr[3], OpHit[3], ~selectWay32bits[3] );
	and andWay4( ctrDcr[4], OpHit[4], ~selectWay32bits[4] );
	and andWay5( ctrDcr[5], OpHit[5], ~selectWay32bits[5] );
	and andWay6( ctrDcr[6], OpHit[6], ~selectWay32bits[6] );
	and andWay7( ctrDcr[7], OpHit[7], ~selectWay32bits[7] );
	
	and andWay8( ctrDcr[8], OpHit[8], ~selectWay32bits[8] );
	and andWay9( ctrDcr[9], OpHit[9], ~selectWay32bits[9] );
	and andWay10( ctrDcr[10], OpHit[10], ~selectWay32bits[10] );
	and andWay11( ctrDcr[11], OpHit[11], ~selectWay32bits[11] );
	and andWay12( ctrDcr[12], OpHit[12], ~selectWay32bits[12] );
	and andWay13( ctrDcr[13], OpHit[13], ~selectWay32bits[13] );
	and andWay14( ctrDcr[14], OpHit[14], ~selectWay32bits[14] );
	and andWay15( ctrDcr[15], OpHit[15], ~selectWay32bits[15] );


	and andWay16( ctrDcr[16], OpHit[16], ~selectWay32bits[16] );
	and andWay17( ctrDcr[17], OpHit[17], ~selectWay32bits[17] );
	and andWay18( ctrDcr[18], OpHit[18], ~selectWay32bits[18] );
	and andWay19( ctrDcr[19], OpHit[19], ~selectWay32bits[19] );
	and andWay20( ctrDcr[20], OpHit[20], ~selectWay32bits[20] );
	and andWay21( ctrDcr[21], OpHit[21], ~selectWay32bits[21] );
	and andWay22( ctrDcr[22], OpHit[22], ~selectWay32bits[22] );
	and andWay23( ctrDcr[23], OpHit[23], ~selectWay32bits[23] );
	
	and andWay24( ctrDcr[24], OpHit[24], ~selectWay32bits[24] );
	and andWay25( ctrDcr[25], OpHit[25], ~selectWay32bits[25] );
	and andWay26( ctrDcr[26], OpHit[26], ~selectWay32bits[26] );
	and andWay27( ctrDcr[27], OpHit[27], ~selectWay32bits[27] );
	and andWay28( ctrDcr[28], OpHit[28], ~selectWay32bits[28] );
	and andWay29( ctrDcr[29], OpHit[29], ~selectWay32bits[29] );
	and andWay30( ctrDcr[30], OpHit[30], ~selectWay32bits[30] );
	and andWay31( ctrDcr[31], OpHit[31], ~selectWay32bits[31] );

	//and andway[31:0]( ctrDcr[31:0] , OpHit[31:0], ~selectWay32bits[31:0] );

	prioEncoderFIFO prioZeroWay( ctrWay[0],ctrWay[1],ctrWay[2],ctrWay[3],ctrWay[4],ctrWay[5],ctrWay[6],ctrWay[7],ctrWay[8],ctrWay[9],ctrWay[10],ctrWay[11],ctrWay[12],ctrWay[13],ctrWay[14],ctrWay[15],ctrWay[16],ctrWay[17],ctrWay[18],ctrWay[19],ctrWay[20],ctrWay[21],ctrWay[22],ctrWay[23],ctrWay[24],ctrWay[25],ctrWay[26],ctrWay[27],ctrWay[28],ctrWay[29],ctrWay[30],ctrWay[31],zeroWay);
	
	always @(*)
   selWay = selectWay5bits;
  
endmodule

module testBenchFIFO();

reg clk;
reg hit;
reg[31:0] vi;
wire [4:0] selWay;
reg reset;

	FIFOCounter fifo(clk, hit, reset, vi,selWay);
	
	
	
	always begin
		#5 clk=~clk;
	end
	
		
	initial
	begin
	clk = 0;
	reset = 1'b1;
	vi = 32'b11111111111111111111111111111111;
	
	#10 reset = 1'b0;

		#10 hit = 1'b1;
		#10 hit = 1'b1;
		#10 hit = 1'b1;
		#10 hit = 1'b1;
		#10 hit = 1'b1;
		#10 hit = 1'b1;
		#10 hit = 1'b1;
		#10 hit = 1'b1;
		#10 hit = 1'b1;
		#10 hit = 1'b1;
		#10 hit = 1'b1;
		
		#10 vi = 32'b01000010000100001000010000100010;
		#10 vi = 32'b01000010000100001000010000100011;
		#10 vi = 32'b11111111111111111111111111111111;
		#10 vi = 32'b01111111111111111111111111111111;
		#10 vi = 32'b11110111111111111111111111111111;
	
		#10$finish;
		
	end
	
	
	
endmodule




