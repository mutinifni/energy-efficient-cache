module D_ff_cache (input clk, input reset, input regWrite, input decOut1b, input init, input d, output reg q);
	always @ (negedge clk)
	begin
	if(reset==1'b1)
		q=init;
	else
		if(regWrite == 1'b1 && decOut1b==1'b1) begin q=d; end
	end
endmodule

module register3bit_mru (input clk, input reset, input regWrite, input decOut1b, input [2:0] init, input [2:0] writeData, output [2:0] outR);
	D_ff_cache d0(clk, reset, regWrite, decOut1b, init[0], writeData[0], outR[0]);
	D_ff_cache d1(clk, reset, regWrite, decOut1b, init[1], writeData[1], outR[1]);
	D_ff_cache d2(clk, reset, regWrite, decOut1b, init[2], writeData[2], outR[2]);
endmodule

module register8bit_cache (input clk, input reset, input regWrite, input decOut1b, input [7:0] init, input [7:0] writeData, output [7:0] outR);
	D_ff_cache d0(clk, reset, regWrite, decOut1b, init[0], writeData[0], outR[0]);
	D_ff_cache d1(clk, reset, regWrite, decOut1b, init[1], writeData[1], outR[1]);
	D_ff_cache d2(clk, reset, regWrite, decOut1b, init[2], writeData[2], outR[2]);
	D_ff_cache d3(clk, reset, regWrite, decOut1b, init[3], writeData[3], outR[3]);
	D_ff_cache d4(clk, reset, regWrite, decOut1b, init[4], writeData[4], outR[4]);
	D_ff_cache d5(clk, reset, regWrite, decOut1b, init[5], writeData[5], outR[5]);
	D_ff_cache d6(clk, reset, regWrite, decOut1b, init[6], writeData[6], outR[6]);
	D_ff_cache d7(clk, reset, regWrite, decOut1b, init[7], writeData[7], outR[7]);
endmodule

module register29bit_cache (input clk, input reset, input regWrite, input decOut1b, input [28:0] init, input [28:0] writeData, output [28:0] outR);
	D_ff_cache d0(clk, reset, regWrite, decOut1b, init[0], writeData[0], outR[0]);
	D_ff_cache d1(clk, reset, regWrite, decOut1b, init[1], writeData[1], outR[1]);
	D_ff_cache d2(clk, reset, regWrite, decOut1b, init[2], writeData[2], outR[2]);
	D_ff_cache d3(clk, reset, regWrite, decOut1b, init[3], writeData[3], outR[3]);
	D_ff_cache d4(clk, reset, regWrite, decOut1b, init[4], writeData[4], outR[4]);
	D_ff_cache d5(clk, reset, regWrite, decOut1b, init[5], writeData[5], outR[5]);
	D_ff_cache d6(clk, reset, regWrite, decOut1b, init[6], writeData[6], outR[6]);
	D_ff_cache d7(clk, reset, regWrite, decOut1b, init[7], writeData[7], outR[7]);
	D_ff_cache d8(clk, reset, regWrite, decOut1b, init[8], writeData[8], outR[8]);
	D_ff_cache d9(clk, reset, regWrite, decOut1b, init[9], writeData[9], outR[9]);
	D_ff_cache d10(clk, reset, regWrite, decOut1b, init[10], writeData[10], outR[10]);
	D_ff_cache d11(clk, reset, regWrite, decOut1b, init[11], writeData[11], outR[11]);
	D_ff_cache d12(clk, reset, regWrite, decOut1b, init[12], writeData[12], outR[12]);
	D_ff_cache d13(clk, reset, regWrite, decOut1b, init[13], writeData[13], outR[13]);
	D_ff_cache d14(clk, reset, regWrite, decOut1b, init[14], writeData[14], outR[14]);
	D_ff_cache d15(clk, reset, regWrite, decOut1b, init[15], writeData[15], outR[15]);
	D_ff_cache d16(clk, reset, regWrite, decOut1b, init[16], writeData[16], outR[16]);
	D_ff_cache d17(clk, reset, regWrite, decOut1b, init[17], writeData[17], outR[17]);
	D_ff_cache d18(clk, reset, regWrite, decOut1b, init[18], writeData[18], outR[18]);
	D_ff_cache d19(clk, reset, regWrite, decOut1b, init[19], writeData[19], outR[19]);
	D_ff_cache d20(clk, reset, regWrite, decOut1b, init[20], writeData[20], outR[20]);
	D_ff_cache d21(clk, reset, regWrite, decOut1b, init[21], writeData[21], outR[21]);
	D_ff_cache d22(clk, reset, regWrite, decOut1b, init[22], writeData[22], outR[22]);
	D_ff_cache d23(clk, reset, regWrite, decOut1b, init[23], writeData[23], outR[23]);
	D_ff_cache d24(clk, reset, regWrite, decOut1b, init[24], writeData[24], outR[24]);
	D_ff_cache d25(clk, reset, regWrite, decOut1b, init[25], writeData[25], outR[25]);
	D_ff_cache d26(clk, reset, regWrite, decOut1b, init[26], writeData[26], outR[26]);
	D_ff_cache d27(clk, reset, regWrite, decOut1b, init[27], writeData[27], outR[27]);
  D_ff_cache d28(clk, reset, regWrite, decOut1b, init[28], writeData[28], outR[28]);
endmodule


module block16byte_cache (input clk, input reset, input regWrite, input decOut1b, input [15:0] decByte, input [7:0] init, input [7:0] writeData, output reg [7:0] outR, output reg [127:0] bigblock);
  wire [127:0] out;
  register8bit_cache b0(clk, reset, regWrite && decByte[0], decOut1b, init, writeData, out[7:0]);
  register8bit_cache b1(clk, reset, regWrite && decByte[1], decOut1b, init, writeData, out[15:8]);
  register8bit_cache b2(clk, reset, regWrite && decByte[2], decOut1b, init, writeData, out[23:16]);
  register8bit_cache b3(clk, reset, regWrite && decByte[3], decOut1b, init, writeData, out[31:24]);
  register8bit_cache b4(clk, reset, regWrite && decByte[4], decOut1b, init , writeData, out[39:32]);
  register8bit_cache b5(clk, reset, regWrite && decByte[5], decOut1b, init , writeData, out[47:40]);
  register8bit_cache b6(clk, reset, regWrite && decByte[6], decOut1b, init , writeData, out[55:48]);
  register8bit_cache b7(clk, reset, regWrite && decByte[7], decOut1b, init , writeData, out[63:56]);
  register8bit_cache b8(clk, reset, regWrite && decByte[8], decOut1b, init, writeData, out[71:64]);
  register8bit_cache b9(clk, reset, regWrite && decByte[9], decOut1b, init , writeData, out[79:72]);
  register8bit_cache b10(clk, reset, regWrite && decByte[10], decOut1b, init, writeData, out[87:80]);
  register8bit_cache b11(clk, reset, regWrite && decByte[11], decOut1b, init, writeData, out[95:88]);
  register8bit_cache b12(clk, reset, regWrite && decByte[12], decOut1b, init, writeData, out[103:96]);
  register8bit_cache b13(clk, reset, regWrite && decByte[13], decOut1b, init, writeData, out[111:104]);
  register8bit_cache b14(clk, reset, regWrite && decByte[14], decOut1b, init, writeData, out[119:112]);
  register8bit_cache b15(clk, reset, regWrite && decByte[15], decOut1b, init, writeData, out[127:120]);
  
  always @ (*)
  begin
  bigblock = out;  
  case(decByte)
      16'b0000000000000001: outR = out[7:0];
			16'b0000000000000010: outR = out[15:8];
			16'b0000000000000100: outR = out[23:16];
			16'b0000000000001000: outR = out[31:24];
			16'b0000000000010000: outR = out[39:32];
			16'b0000000000100000: outR = out[47:40];
			16'b0000000001000000: outR = out[55:48];
			16'b0000000010000000: outR = out[63:56];
			16'b0000000100000000: outR = out[71:64];
			16'b0000001000000000: outR = out[79:72];
			16'b0000010000000000: outR = out[87:80];
			16'b0000100000000000: outR = out[95:88];
			16'b0001000000000000: outR = out[103:96];
			16'b0010000000000000: outR = out[111:104];
			16'b0100000000000000: outR = out[119:112];
			16'b1000000000000000: outR = out[127:120];
  endcase
  end
endmodule

module cacheline (input clk, reset, validWrite, dirtyWrite, tagWrite, input blockWrite, input decOut1b, input [15:0] decByte, input [38:0] init,
                  input validWriteData, dirtyWriteData, input [28:0] tagWriteData, input [7:0] blockWriteData,
                  output validOut, dirtyOut, output [28:0] tagOut, output [7:0] blockOut, output [127:0] bigblock);
  D_ff_cache valid(clk, reset, validWrite, decOut1b, init[38], validWriteData, validOut);
  D_ff_cache dirty(clk, reset, dirtyWrite, decOut1b, init[37], dirtyWriteData, dirtyOut);
  register29bit_cache tag(clk, reset, tagWrite, decOut1b, init[36:8],tagWriteData, tagOut);
  block16byte_cache block(clk, reset, blockWrite, decOut1b, decByte, init[7:0], blockWriteData, blockOut, bigblock);
endmodule

module cache_way0 (input clk, reset, input validWrite, input dirtyWrite, input tagWrite, input blockWrite, input [31:0] decOut1b,
                  input [15:0] decByte, input validWriteData, dirtyWriteData, input [28:0] tagWriteData,
                  input [7:0] blockWriteData, output way_valid, output way_dirty, output [28:0] way_tag, output [7:0] way_data, output [127:0] bigblock);
  wire [31:0] validOut;
  wire [31:0] dirtyOut;
  wire [28:0] tagOut0,tagOut1,tagOut2,tagOut3,tagOut4,tagOut5,tagOut6,tagOut7,tagOut8,tagOut9,tagOut10,tagOut11,tagOut12,
              tagOut13,tagOut14,tagOut15,tagOut16,tagOut17,tagOut18,tagOut19,tagOut20,tagOut21,tagOut22,tagOut23,tagOut24,tagOut25,
              tagOut26,tagOut27,tagOut28,tagOut29,tagOut30,tagOut31;
  wire [7:0]  blockOut0,blockOut1,blockOut2,blockOut3,blockOut4,blockOut5,blockOut6,blockOut7,blockOut8,blockOut9,
              blockOut10,blockOut11,blockOut12,blockOut13,blockOut14,blockOut15,blockOut16,blockOut17,blockOut18,blockOut19,blockOut20,
              blockOut21,blockOut22,blockOut23,blockOut24,blockOut25,blockOut26,blockOut27,blockOut28,blockOut29,blockOut30,blockOut31;
  wire [127:0]  bigblock0,bigblock1,bigblock2,bigblock3,bigblock4,bigblock5,bigblock6,bigblock7,bigblock8,bigblock9,
              bigblock10,bigblock11,bigblock12,bigblock13,bigblock14,bigblock15,bigblock16,bigblock17,bigblock18,bigblock19,bigblock20,
              bigblock21,bigblock22,bigblock23,bigblock24,bigblock25,bigblock26,bigblock27,bigblock28,bigblock29,bigblock30,bigblock31;
  cacheline c0 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[0], decByte, 39'b1_1_1111_1111_1111_1111_1111_1111_1111_1111_1111_1,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[0], dirtyOut[0], tagOut0, blockOut0, bigblock0);
  cacheline c1 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[1], decByte, 39'b1_0_0000_0000_0000_0000_0000_0000_0000_1011_1101_1,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[1], dirtyOut[1], tagOut1, blockOut1, bigblock1);
  cacheline c2 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[2], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[2], dirtyOut[2], tagOut2, blockOut2, bigblock2);
  cacheline c3 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[3], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[3], dirtyOut[3], tagOut3, blockOut3, bigblock3);
  cacheline c4 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[4], decByte, 39'b0,
               validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[4], dirtyOut[4], tagOut4, blockOut4, bigblock4);
  cacheline c5 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[5], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[5], dirtyOut[5], tagOut5, blockOut5, bigblock5);
  cacheline c6 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[6], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[6], dirtyOut[6], tagOut6, blockOut6, bigblock6);
  cacheline c7 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[7], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[7], dirtyOut[7], tagOut7, blockOut7, bigblock7);
  cacheline c8 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[8], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[8], dirtyOut[8], tagOut8, blockOut8, bigblock8);
  cacheline c9 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[9], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[9], dirtyOut[9], tagOut9, blockOut9, bigblock9);
  cacheline c10 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[10], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[10], dirtyOut[10], tagOut10, blockOut10, bigblock10);
  cacheline c11 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[11], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[11], dirtyOut[11], tagOut11, blockOut11, bigblock11);
  cacheline c12 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[12], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[12], dirtyOut[12], tagOut12, blockOut12, bigblock12);
  cacheline c13 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[13], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[13], dirtyOut[13], tagOut13, blockOut13, bigblock13);
  cacheline c14 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[14], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[14], dirtyOut[14], tagOut14, blockOut14, bigblock14);
  cacheline c15 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[15], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[15], dirtyOut[15], tagOut15, blockOut15, bigblock15);
  cacheline c16 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[16], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[16], dirtyOut[16], tagOut16, blockOut16, bigblock16);
  cacheline c17 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[17], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[17], dirtyOut[17], tagOut17, blockOut17, bigblock17);
  cacheline c18 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[18], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[18], dirtyOut[18], tagOut18, blockOut18, bigblock18);
  cacheline c19 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[19], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[19], dirtyOut[19], tagOut19, blockOut19, bigblock19);
  cacheline c20 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[20], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[20], dirtyOut[20], tagOut20, blockOut20, bigblock20);
  cacheline c21 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[21], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[21], dirtyOut[21], tagOut21, blockOut21, bigblock21);
  cacheline c22 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[22], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[22], dirtyOut[22], tagOut22, blockOut22, bigblock22);
  cacheline c23 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[23], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[23], dirtyOut[23], tagOut23, blockOut23, bigblock23);
  cacheline c24 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[24], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[24], dirtyOut[24], tagOut24, blockOut24, bigblock24);
  cacheline c25 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[25], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[25], dirtyOut[25], tagOut25, blockOut25, bigblock25);
  cacheline c26 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[26], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[26], dirtyOut[26], tagOut26, blockOut26, bigblock26);
  cacheline c27 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[27], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[27], dirtyOut[27], tagOut27, blockOut27, bigblock27);
  cacheline c28 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[28], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[28], dirtyOut[28], tagOut28, blockOut28, bigblock28);
  cacheline c29 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[29], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[29], dirtyOut[29], tagOut29, blockOut29, bigblock29);
  cacheline c30 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[30], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[30], dirtyOut[30], tagOut30, blockOut30, bigblock30);
  cacheline c31 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[31], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[31], dirtyOut[31], tagOut31, blockOut31, bigblock31);                
  tristate_39bit t0 ({validOut[0], dirtyOut[0], tagOut0, blockOut0}, decOut1b[0], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t1 ({validOut[1], dirtyOut[1], tagOut1, blockOut1}, decOut1b[1], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t2 ({validOut[2], dirtyOut[2], tagOut2, blockOut2}, decOut1b[2], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t3 ({validOut[3], dirtyOut[3], tagOut3, blockOut3}, decOut1b[3], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t4 ({validOut[4], dirtyOut[4], tagOut4, blockOut4}, decOut1b[4], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t5 ({validOut[5], dirtyOut[5], tagOut5, blockOut5}, decOut1b[5], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t6 ({validOut[6], dirtyOut[6], tagOut6, blockOut6}, decOut1b[6], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t7 ({validOut[7], dirtyOut[7], tagOut7, blockOut7}, decOut1b[7], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t8 ({validOut[8], dirtyOut[8], tagOut8, blockOut8}, decOut1b[8], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t9 ({validOut[9], dirtyOut[9], tagOut9, blockOut9}, decOut1b[9], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t10 ({validOut[10], dirtyOut[10], tagOut10, blockOut10}, decOut1b[10], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t11 ({validOut[11], dirtyOut[11], tagOut11, blockOut11}, decOut1b[11], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t12 ({validOut[12], dirtyOut[12], tagOut12, blockOut12}, decOut1b[12], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t13 ({validOut[13], dirtyOut[13], tagOut13, blockOut13}, decOut1b[13], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t14 ({validOut[14], dirtyOut[14], tagOut14, blockOut14}, decOut1b[14], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t15 ({validOut[15], dirtyOut[15], tagOut15, blockOut15}, decOut1b[15], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t16 ({validOut[16], dirtyOut[16], tagOut16, blockOut16}, decOut1b[16], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t17 ({validOut[17], dirtyOut[17], tagOut17, blockOut17}, decOut1b[17], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t18 ({validOut[18], dirtyOut[18], tagOut18, blockOut18}, decOut1b[18], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t19 ({validOut[19], dirtyOut[19], tagOut19, blockOut19}, decOut1b[19], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t20 ({validOut[20], dirtyOut[20], tagOut20, blockOut20}, decOut1b[20], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t21 ({validOut[21], dirtyOut[21], tagOut21, blockOut21}, decOut1b[21], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t22 ({validOut[22], dirtyOut[22], tagOut22, blockOut22}, decOut1b[22], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t23 ({validOut[23], dirtyOut[23], tagOut23, blockOut23}, decOut1b[23], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t24 ({validOut[24], dirtyOut[24], tagOut24, blockOut24}, decOut1b[24], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t25 ({validOut[25], dirtyOut[25], tagOut25, blockOut25}, decOut1b[25], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t26 ({validOut[26], dirtyOut[26], tagOut26, blockOut26}, decOut1b[26], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t27 ({validOut[27], dirtyOut[27], tagOut27, blockOut27}, decOut1b[27], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t28 ({validOut[28], dirtyOut[28], tagOut28, blockOut28}, decOut1b[28], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t29 ({validOut[29], dirtyOut[29], tagOut29, blockOut29}, decOut1b[29], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t30 ({validOut[30], dirtyOut[30], tagOut30, blockOut30}, decOut1b[30], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t31 ({validOut[31], dirtyOut[31], tagOut31, blockOut31}, decOut1b[31], {way_valid, way_dirty, way_tag, way_data});

  tristate_128bit ts0(bigblock0, decOut1b[0], bigblock);
  tristate_128bit ts1(bigblock1, decOut1b[1], bigblock);
  tristate_128bit ts2(bigblock2, decOut1b[2], bigblock);
  tristate_128bit ts3(bigblock3, decOut1b[3], bigblock);
  tristate_128bit ts4(bigblock4, decOut1b[4], bigblock);
  tristate_128bit ts5(bigblock5, decOut1b[5], bigblock);
  tristate_128bit ts6(bigblock6, decOut1b[6], bigblock);
  tristate_128bit ts7(bigblock7, decOut1b[7], bigblock);
  tristate_128bit ts8(bigblock8, decOut1b[8], bigblock);
  tristate_128bit ts9(bigblock9, decOut1b[9], bigblock);
  tristate_128bit ts10(bigblock10, decOut1b[10], bigblock);
  tristate_128bit ts11(bigblock11, decOut1b[11], bigblock);
  tristate_128bit ts12(bigblock12, decOut1b[12], bigblock);
  tristate_128bit ts13(bigblock13, decOut1b[13], bigblock);
  tristate_128bit ts14(bigblock14, decOut1b[14], bigblock);
  tristate_128bit ts15(bigblock15, decOut1b[15], bigblock);
  tristate_128bit ts16(bigblock16, decOut1b[16], bigblock);
  tristate_128bit ts17(bigblock17, decOut1b[17], bigblock);
  tristate_128bit ts18(bigblock18, decOut1b[18], bigblock);
  tristate_128bit ts19(bigblock19, decOut1b[19], bigblock);
  tristate_128bit ts20(bigblock20, decOut1b[20], bigblock);
  tristate_128bit ts21(bigblock21, decOut1b[21], bigblock);
  tristate_128bit ts22(bigblock22, decOut1b[22], bigblock);
  tristate_128bit ts23(bigblock23, decOut1b[23], bigblock);
  tristate_128bit ts24(bigblock24, decOut1b[24], bigblock);
  tristate_128bit ts25(bigblock25, decOut1b[25], bigblock);
  tristate_128bit ts26(bigblock26, decOut1b[26], bigblock);
  tristate_128bit ts27(bigblock27, decOut1b[27], bigblock);
  tristate_128bit ts28(bigblock28, decOut1b[28], bigblock);
  tristate_128bit ts29(bigblock29, decOut1b[29], bigblock);
  tristate_128bit ts30(bigblock30, decOut1b[30], bigblock);
  tristate_128bit ts31(bigblock31, decOut1b[31], bigblock);
endmodule

module cache_way (input clk, reset, input validWrite, input dirtyWrite, input tagWrite, input blockWrite, input [31:0] decOut1b,
                  input [15:0] decByte, input validWriteData, dirtyWriteData, input [28:0] tagWriteData,
                  input [7:0] blockWriteData, output way_valid, output way_dirty, output [28:0] way_tag, output [7:0] way_data, output [127:0] bigblock);
  wire [31:0] validOut;
  wire [31:0] dirtyOut;
  wire [28:0] tagOut0,tagOut1,tagOut2,tagOut3,tagOut4,tagOut5,tagOut6,tagOut7,tagOut8,tagOut9,tagOut10,tagOut11,tagOut12,
              tagOut13,tagOut14,tagOut15,tagOut16,tagOut17,tagOut18,tagOut19,tagOut20,tagOut21,tagOut22,tagOut23,tagOut24,tagOut25,
              tagOut26,tagOut27,tagOut28,tagOut29,tagOut30,tagOut31;
  wire [7:0]  blockOut0,blockOut1,blockOut2,blockOut3,blockOut4,blockOut5,blockOut6,blockOut7,blockOut8,blockOut9,
              blockOut10,blockOut11,blockOut12,blockOut13,blockOut14,blockOut15,blockOut16,blockOut17,blockOut18,blockOut19,blockOut20,
              blockOut21,blockOut22,blockOut23,blockOut24,blockOut25,blockOut26,blockOut27,blockOut28,blockOut29,blockOut30,blockOut31;
  wire [127:0]  bigblock0,bigblock1,bigblock2,bigblock3,bigblock4,bigblock5,bigblock6,bigblock7,bigblock8,bigblock9,
              bigblock10,bigblock11,bigblock12,bigblock13,bigblock14,bigblock15,bigblock16,bigblock17,bigblock18,bigblock19,bigblock20,
              bigblock21,bigblock22,bigblock23,bigblock24,bigblock25,bigblock26,bigblock27,bigblock28,bigblock29,bigblock30,bigblock31;
  cacheline c0 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[0], decByte, 39'b0_0_1111_1111_1111_1111_1111_1111_1111_1111_1111_1,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[0], dirtyOut[0], tagOut0, blockOut0, bigblock0);
  cacheline c1 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[1], decByte, 39'b0_0_0000_0000_0000_0000_0000_0000_0000_1011_1101_1,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[1], dirtyOut[1], tagOut1, blockOut1, bigblock1);
  cacheline c2 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[2], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[2], dirtyOut[2], tagOut2, blockOut2, bigblock2);
  cacheline c3 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[3], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[3], dirtyOut[3], tagOut3, blockOut3, bigblock3);
  cacheline c4 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[4], decByte, 39'b0,
               validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[4], dirtyOut[4], tagOut4, blockOut4, bigblock4);
  cacheline c5 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[5], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[5], dirtyOut[5], tagOut5, blockOut5, bigblock5);
  cacheline c6 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[6], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[6], dirtyOut[6], tagOut6, blockOut6, bigblock6);
  cacheline c7 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[7], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[7], dirtyOut[7], tagOut7, blockOut7, bigblock7);
  cacheline c8 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[8], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[8], dirtyOut[8], tagOut8, blockOut8, bigblock8);
  cacheline c9 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[9], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[9], dirtyOut[9], tagOut9, blockOut9, bigblock9);
  cacheline c10 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[10], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[10], dirtyOut[10], tagOut10, blockOut10, bigblock10);
  cacheline c11 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[11], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[11], dirtyOut[11], tagOut11, blockOut11, bigblock11);
  cacheline c12 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[12], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[12], dirtyOut[12], tagOut12, blockOut12, bigblock12);
  cacheline c13 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[13], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[13], dirtyOut[13], tagOut13, blockOut13, bigblock13);
  cacheline c14 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[14], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[14], dirtyOut[14], tagOut14, blockOut14, bigblock14);
  cacheline c15 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[15], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[15], dirtyOut[15], tagOut15, blockOut15, bigblock15);
  cacheline c16 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[16], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[16], dirtyOut[16], tagOut16, blockOut16, bigblock16);
  cacheline c17 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[17], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[17], dirtyOut[17], tagOut17, blockOut17, bigblock17);
  cacheline c18 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[18], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[18], dirtyOut[18], tagOut18, blockOut18, bigblock18);
  cacheline c19 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[19], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[19], dirtyOut[19], tagOut19, blockOut19, bigblock19);
  cacheline c20 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[20], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[20], dirtyOut[20], tagOut20, blockOut20, bigblock20);
  cacheline c21 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[21], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[21], dirtyOut[21], tagOut21, blockOut21, bigblock21);
  cacheline c22 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[22], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[22], dirtyOut[22], tagOut22, blockOut22, bigblock22);
  cacheline c23 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[23], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[23], dirtyOut[23], tagOut23, blockOut23, bigblock23);
  cacheline c24 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[24], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[24], dirtyOut[24], tagOut24, blockOut24, bigblock24);
  cacheline c25 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[25], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[25], dirtyOut[25], tagOut25, blockOut25, bigblock25);
  cacheline c26 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[26], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[26], dirtyOut[26], tagOut26, blockOut26, bigblock26);
  cacheline c27 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[27], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[27], dirtyOut[27], tagOut27, blockOut27, bigblock27);
  cacheline c28 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[28], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[28], dirtyOut[28], tagOut28, blockOut28, bigblock28);
  cacheline c29 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[29], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[29], dirtyOut[29], tagOut29, blockOut29, bigblock29);
  cacheline c30 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[30], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[30], dirtyOut[30], tagOut30, blockOut30, bigblock30);
  cacheline c31 (clk, reset, validWrite, dirtyWrite, tagWrite, blockWrite, decOut1b[31], decByte, 39'b0,
                validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                validOut[31], dirtyOut[31], tagOut31, blockOut31, bigblock31);                
  tristate_39bit t0 ({validOut[0], dirtyOut[0], tagOut0, blockOut0}, decOut1b[0], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t1 ({validOut[1], dirtyOut[1], tagOut1, blockOut1}, decOut1b[1], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t2 ({validOut[2], dirtyOut[2], tagOut2, blockOut2}, decOut1b[2], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t3 ({validOut[3], dirtyOut[3], tagOut3, blockOut3}, decOut1b[3], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t4 ({validOut[4], dirtyOut[4], tagOut4, blockOut4}, decOut1b[4], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t5 ({validOut[5], dirtyOut[5], tagOut5, blockOut5}, decOut1b[5], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t6 ({validOut[6], dirtyOut[6], tagOut6, blockOut6}, decOut1b[6], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t7 ({validOut[7], dirtyOut[7], tagOut7, blockOut7}, decOut1b[7], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t8 ({validOut[8], dirtyOut[8], tagOut8, blockOut8}, decOut1b[8], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t9 ({validOut[9], dirtyOut[9], tagOut9, blockOut9}, decOut1b[9], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t10 ({validOut[10], dirtyOut[10], tagOut10, blockOut10}, decOut1b[10], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t11 ({validOut[11], dirtyOut[11], tagOut11, blockOut11}, decOut1b[11], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t12 ({validOut[12], dirtyOut[12], tagOut12, blockOut12}, decOut1b[12], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t13 ({validOut[13], dirtyOut[13], tagOut13, blockOut13}, decOut1b[13], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t14 ({validOut[14], dirtyOut[14], tagOut14, blockOut14}, decOut1b[14], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t15 ({validOut[15], dirtyOut[15], tagOut15, blockOut15}, decOut1b[15], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t16 ({validOut[16], dirtyOut[16], tagOut16, blockOut16}, decOut1b[16], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t17 ({validOut[17], dirtyOut[17], tagOut17, blockOut17}, decOut1b[17], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t18 ({validOut[18], dirtyOut[18], tagOut18, blockOut18}, decOut1b[18], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t19 ({validOut[19], dirtyOut[19], tagOut19, blockOut19}, decOut1b[19], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t20 ({validOut[20], dirtyOut[20], tagOut20, blockOut20}, decOut1b[20], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t21 ({validOut[21], dirtyOut[21], tagOut21, blockOut21}, decOut1b[21], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t22 ({validOut[22], dirtyOut[22], tagOut22, blockOut22}, decOut1b[22], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t23 ({validOut[23], dirtyOut[23], tagOut23, blockOut23}, decOut1b[23], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t24 ({validOut[24], dirtyOut[24], tagOut24, blockOut24}, decOut1b[24], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t25 ({validOut[25], dirtyOut[25], tagOut25, blockOut25}, decOut1b[25], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t26 ({validOut[26], dirtyOut[26], tagOut26, blockOut26}, decOut1b[26], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t27 ({validOut[27], dirtyOut[27], tagOut27, blockOut27}, decOut1b[27], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t28 ({validOut[28], dirtyOut[28], tagOut28, blockOut28}, decOut1b[28], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t29 ({validOut[29], dirtyOut[29], tagOut29, blockOut29}, decOut1b[29], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t30 ({validOut[30], dirtyOut[30], tagOut30, blockOut30}, decOut1b[30], {way_valid, way_dirty, way_tag, way_data});
  tristate_39bit t31 ({validOut[31], dirtyOut[31], tagOut31, blockOut31}, decOut1b[31], {way_valid, way_dirty, way_tag, way_data});

  tristate_128bit ts0(bigblock0, decOut1b[0], bigblock);
  tristate_128bit ts1(bigblock1, decOut1b[1], bigblock);
  tristate_128bit ts2(bigblock2, decOut1b[2], bigblock);
  tristate_128bit ts3(bigblock3, decOut1b[3], bigblock);
  tristate_128bit ts4(bigblock4, decOut1b[4], bigblock);
  tristate_128bit ts5(bigblock5, decOut1b[5], bigblock);
  tristate_128bit ts6(bigblock6, decOut1b[6], bigblock);
  tristate_128bit ts7(bigblock7, decOut1b[7], bigblock);
  tristate_128bit ts8(bigblock8, decOut1b[8], bigblock);
  tristate_128bit ts9(bigblock9, decOut1b[9], bigblock);
  tristate_128bit ts10(bigblock10, decOut1b[10], bigblock);
  tristate_128bit ts11(bigblock11, decOut1b[11], bigblock);
  tristate_128bit ts12(bigblock12, decOut1b[12], bigblock);
  tristate_128bit ts13(bigblock13, decOut1b[13], bigblock);
  tristate_128bit ts14(bigblock14, decOut1b[14], bigblock);
  tristate_128bit ts15(bigblock15, decOut1b[15], bigblock);
  tristate_128bit ts16(bigblock16, decOut1b[16], bigblock);
  tristate_128bit ts17(bigblock17, decOut1b[17], bigblock);
  tristate_128bit ts18(bigblock18, decOut1b[18], bigblock);
  tristate_128bit ts19(bigblock19, decOut1b[19], bigblock);
  tristate_128bit ts20(bigblock20, decOut1b[20], bigblock);
  tristate_128bit ts21(bigblock21, decOut1b[21], bigblock);
  tristate_128bit ts22(bigblock22, decOut1b[22], bigblock);
  tristate_128bit ts23(bigblock23, decOut1b[23], bigblock);
  tristate_128bit ts24(bigblock24, decOut1b[24], bigblock);
  tristate_128bit ts25(bigblock25, decOut1b[25], bigblock);
  tristate_128bit ts26(bigblock26, decOut1b[26], bigblock);
  tristate_128bit ts27(bigblock27, decOut1b[27], bigblock);
  tristate_128bit ts28(bigblock28, decOut1b[28], bigblock);
  tristate_128bit ts29(bigblock29, decOut1b[29], bigblock);
  tristate_128bit ts30(bigblock30, decOut1b[30], bigblock);
  tristate_128bit ts31(bigblock31, decOut1b[31], bigblock);
endmodule

module mru_array (input clk, reset, mruRegWrite, input [31:0] decOut1b, input [2:0] mruWriteData, output [2:0] mru);
  wire [2:0] mruOut0,mruOut1,mruOut2,mruOut3,mruOut4,mruOut5,mruOut6,mruOut7,mruOut8,mruOut9,mruOut10,mruOut11,mruOut12,
             mruOut13,mruOut14,mruOut15,mruOut16,mruOut17,mruOut18,mruOut19,mruOut20,mruOut21,mruOut22,mruOut23,mruOut24,mruOut25,
             mruOut26,mruOut27,mruOut28,mruOut29,mruOut30,mruOut31;
  register3bit_mru r0(clk, reset, mruRegWrite, decOut1b[0], 3'b001, mruWriteData, mruOut0);
  register3bit_mru r1(clk, reset, mruRegWrite, decOut1b[1], 3'b000,mruWriteData, mruOut1);
  register3bit_mru r2(clk, reset, mruRegWrite, decOut1b[2], 3'b000,mruWriteData, mruOut2);
  register3bit_mru r3(clk, reset, mruRegWrite, decOut1b[3], 3'b000,mruWriteData, mruOut3);
  register3bit_mru r4(clk, reset, mruRegWrite, decOut1b[4], 3'b000,mruWriteData, mruOut4);
  register3bit_mru r5(clk, reset, mruRegWrite, decOut1b[5], 3'b000,mruWriteData, mruOut5);
  register3bit_mru r6(clk, reset, mruRegWrite, decOut1b[6], 3'b000,mruWriteData, mruOut6);
  register3bit_mru r7(clk, reset, mruRegWrite, decOut1b[7], 3'b000,mruWriteData, mruOut7);
  register3bit_mru r8(clk, reset, mruRegWrite, decOut1b[8], 3'b000,mruWriteData, mruOut8);
  register3bit_mru r9(clk, reset, mruRegWrite, decOut1b[9], 3'b000,mruWriteData, mruOut9);
  register3bit_mru r10(clk, reset, mruRegWrite, decOut1b[10],3'b000, mruWriteData, mruOut10);
  register3bit_mru r11(clk, reset, mruRegWrite, decOut1b[11], 3'b000,mruWriteData, mruOut11);
  register3bit_mru r12(clk, reset, mruRegWrite, decOut1b[12],3'b000, mruWriteData, mruOut12);
  register3bit_mru r13(clk, reset, mruRegWrite, decOut1b[13], 3'b000,mruWriteData, mruOut13);
  register3bit_mru r14(clk, reset, mruRegWrite, decOut1b[14], 3'b000,mruWriteData, mruOut14);
  register3bit_mru r15(clk, reset, mruRegWrite, decOut1b[15],3'b000, mruWriteData, mruOut15);
  register3bit_mru r16(clk, reset, mruRegWrite, decOut1b[16],3'b000, mruWriteData, mruOut16);
  register3bit_mru r17(clk, reset, mruRegWrite, decOut1b[17],3'b000, mruWriteData, mruOut17);
  register3bit_mru r18(clk, reset, mruRegWrite, decOut1b[18], 3'b000,mruWriteData, mruOut18);
  register3bit_mru r19(clk, reset, mruRegWrite, decOut1b[19],3'b000, mruWriteData, mruOut19);
  register3bit_mru r20(clk, reset, mruRegWrite, decOut1b[20], 3'b000,mruWriteData, mruOut20);
  register3bit_mru r21(clk, reset, mruRegWrite, decOut1b[21], 3'b000,mruWriteData, mruOut21);
  register3bit_mru r22(clk, reset, mruRegWrite, decOut1b[22],3'b000, mruWriteData, mruOut22);
  register3bit_mru r23(clk, reset, mruRegWrite, decOut1b[23],3'b000, mruWriteData, mruOut23);
  register3bit_mru r24(clk, reset, mruRegWrite, decOut1b[24],3'b000, mruWriteData, mruOut24);
  register3bit_mru r25(clk, reset, mruRegWrite, decOut1b[25],3'b000, mruWriteData, mruOut25);
  register3bit_mru r26(clk, reset, mruRegWrite, decOut1b[26],3'b000, mruWriteData, mruOut26);
  register3bit_mru r27(clk, reset, mruRegWrite, decOut1b[27],3'b000, mruWriteData, mruOut27);
  register3bit_mru r28(clk, reset, mruRegWrite, decOut1b[28],3'b000, mruWriteData, mruOut28);
  register3bit_mru r29(clk, reset, mruRegWrite, decOut1b[29],3'b000, mruWriteData, mruOut29);
  register3bit_mru r30(clk, reset, mruRegWrite, decOut1b[30],3'b000, mruWriteData, mruOut30);
  register3bit_mru r31(clk, reset, mruRegWrite, decOut1b[31],3'b000, mruWriteData, mruOut31);
  
  tristate_3bit t0 (mruOut0, decOut1b[0], mru);
  tristate_3bit t1 (mruOut1, decOut1b[1], mru);
  tristate_3bit t2 (mruOut2, decOut1b[2], mru);
  tristate_3bit t3 (mruOut3, decOut1b[3], mru);
  tristate_3bit t4 (mruOut4, decOut1b[4], mru);
  tristate_3bit t5 (mruOut5, decOut1b[5], mru);
  tristate_3bit t6 (mruOut6, decOut1b[6], mru);
  tristate_3bit t7 (mruOut7, decOut1b[7], mru);
  tristate_3bit t8 (mruOut8, decOut1b[8], mru);
  tristate_3bit t9 (mruOut9, decOut1b[9], mru);
  tristate_3bit t10 (mruOut10, decOut1b[10], mru);
  tristate_3bit t11 (mruOut11, decOut1b[11], mru);
  tristate_3bit t12 (mruOut12, decOut1b[12], mru);
  tristate_3bit t13 (mruOut13, decOut1b[13], mru);
  tristate_3bit t14 (mruOut14, decOut1b[14], mru);
  tristate_3bit t15 (mruOut15, decOut1b[15], mru);
  tristate_3bit t16 (mruOut16, decOut1b[16], mru);
  tristate_3bit t17 (mruOut17, decOut1b[17], mru);
  tristate_3bit t18 (mruOut18, decOut1b[18], mru);
  tristate_3bit t19 (mruOut19, decOut1b[19], mru);
  tristate_3bit t20 (mruOut20, decOut1b[20], mru);
  tristate_3bit t21 (mruOut21, decOut1b[21], mru);
  tristate_3bit t22 (mruOut22, decOut1b[22], mru);
  tristate_3bit t23 (mruOut23, decOut1b[23], mru);
  tristate_3bit t24 (mruOut24, decOut1b[24], mru);
  tristate_3bit t25 (mruOut25, decOut1b[25], mru);
  tristate_3bit t26 (mruOut26, decOut1b[26], mru);
  tristate_3bit t27 (mruOut27, decOut1b[27], mru);
  tristate_3bit t28 (mruOut28, decOut1b[28], mru);
  tristate_3bit t29 (mruOut29, decOut1b[29], mru);
  tristate_3bit t30 (mruOut30, decOut1b[30], mru);
  tristate_3bit t31 (mruOut31, decOut1b[31], mru);
endmodule

module decoder3to8 (input [2:0] decInp, output reg [7:0] decOut);
	always@(decInp)
	begin
	case(decInp)
			3'b000: decOut=8'b00000001; 
			3'b001: decOut=8'b00000010;
			3'b010: decOut=8'b00000100;
			3'b011: decOut=8'b00001000;
			3'b100: decOut=8'b00010000;
			3'b101: decOut=8'b00100000;
			3'b110: decOut=8'b01000000;
			3'b111: decOut=8'b10000000;
		endcase
		end
endmodule

module decoder4to16 (input [3:0] decInp, output reg [15:0] decOut);
	always@(decInp)
	case(decInp)
			4'b0000: decOut=16'b0000000000000001; 
			4'b0001: decOut=16'b0000000000000010;
			4'b0010: decOut=16'b0000000000000100;
			4'b0011: decOut=16'b0000000000001000;
			4'b0100: decOut=16'b0000000000010000;
			4'b0101: decOut=16'b0000000000100000;
			4'b0110: decOut=16'b0000000001000000;
			4'b0111: decOut=16'b0000000010000000;
			4'b1000: decOut=16'b0000000100000000; 
			4'b1001: decOut=16'b0000001000000000;
			4'b1010: decOut=16'b0000010000000000;
			4'b1011: decOut=16'b0000100000000000;
			4'b1100: decOut=16'b0001000000000000;
			4'b1101: decOut=16'b0010000000000000;
			4'b1110: decOut=16'b0100000000000000;
			4'b1111: decOut=16'b1000000000000000;
	endcase
endmodule

module decoder5to32 (input [4:0] decInp, output reg [31:0] decOut);
	always@(decInp)
	begin
	case(decInp)
	    5'b00000: decOut = 32'b00000000000000000000000000000001;
	    5'b00001: decOut = 32'b00000000000000000000000000000010;
	    5'b00010: decOut = 32'b00000000000000000000000000000100;
	    5'b00011: decOut = 32'b00000000000000000000000000001000;
	    5'b00100: decOut = 32'b00000000000000000000000000010000;
	    5'b00101: decOut = 32'b00000000000000000000000000100000;
	    5'b00110: decOut = 32'b00000000000000000000000001000000;
	    5'b00111: decOut = 32'b00000000000000000000000010000000;
	    5'b01000: decOut = 32'b00000000000000000000000100000000;
	    5'b01001: decOut = 32'b00000000000000000000001000000000;
	    5'b01010: decOut = 32'b00000000000000000000010000000000;
	    5'b01011: decOut = 32'b00000000000000000000100000000000;
	    5'b01100: decOut = 32'b00000000000000000001000000000000;
	    5'b01101: decOut = 32'b00000000000000000010000000000000;
	    5'b01110: decOut = 32'b00000000000000000100000000000000;
	    5'b01111: decOut = 32'b00000000000000001000000000000000;
	    5'b10000: decOut = 32'b00000000000000010000000000000000;
	    5'b10001: decOut = 32'b00000000000000100000000000000000;
	    5'b10010: decOut = 32'b00000000000001000000000000000000;
	    5'b10011: decOut = 32'b00000000000010000000000000000000;
	    5'b10100: decOut = 32'b00000000000100000000000000000000;
	    5'b10101: decOut = 32'b00000000001000000000000000000000;
	    5'b10110: decOut = 32'b00000000010000000000000000000000;
	    5'b10111: decOut = 32'b00000000100000000000000000000000;
	    5'b11000: decOut = 32'b00000001000000000000000000000000;
	    5'b11001: decOut = 32'b00000010000000000000000000000000;
	    5'b11010: decOut = 32'b00000100000000000000000000000000;
	    5'b11011: decOut = 32'b00001000000000000000000000000000;
	    5'b11100: decOut = 32'b00010000000000000000000000000000;
	    5'b11101: decOut = 32'b00100000000000000000000000000000;
	    5'b11110: decOut = 32'b01000000000000000000000000000000;
	    5'b11111: decOut = 32'b10000000000000000000000000000000;
			endcase
		end
endmodule

module mux32to1_3bit (input [2:0] outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7,outR8,outR9,outR10,outR11,outR12,outR13,outR14,outR15,
                             outR16,outR17,outR18,outR19,outR20,outR21,outR22,outR23,outR24,outR25,outR26,outR27,outR28,outR29,outR30,outR31,
                      input [4:0] sel, output reg [2:0] muxOut);
always @ (outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7,outR8,outR9,outR10,outR11,outR12,outR13,outR14,outR15,
          outR16,outR17,outR18,outR19,outR20,outR21,outR22,outR23,outR24,outR25,outR26,outR27,outR28,outR29,outR30,outR31,sel)
  begin
    case(sel)
	    5'b00000: muxOut = outR0;
	    5'b00001: muxOut = outR1;
	    5'b00010: muxOut = outR2;
	    5'b00011: muxOut = outR3;
	    5'b00100: muxOut = outR4;
	    5'b00101: muxOut = outR5;
	    5'b00110: muxOut = outR6;
	    5'b00111: muxOut = outR7;
	    5'b01000: muxOut = outR8;
	    5'b01001: muxOut = outR9;
	    5'b01010: muxOut = outR10;
	    5'b01011: muxOut = outR11;
	    5'b01100: muxOut = outR12;
	    5'b01101: muxOut = outR13;
	    5'b01110: muxOut = outR14;
	    5'b01111: muxOut = outR15;
	    5'b10000: muxOut = outR16;
	    5'b10001: muxOut = outR17;
	    5'b10010: muxOut = outR18;
	    5'b10011: muxOut = outR19;
	    5'b10100: muxOut = outR20;
	    5'b10101: muxOut = outR21;
	    5'b10110: muxOut = outR22;
	    5'b10111: muxOut = outR23;
	    5'b11000: muxOut = outR24;
	    5'b11001: muxOut = outR25;
	    5'b11010: muxOut = outR26;
	    5'b11011: muxOut = outR27;
	    5'b11100: muxOut = outR28;
	    5'b11101: muxOut = outR29;
	    5'b11110: muxOut = outR30;
	    5'b11111: muxOut = outR31;
    endcase
  end  
endmodule

module mux2to1_1bit (input inp1, input inp2, input sel, output reg muxOut);
  always @ (inp1, inp2, sel)
  begin
    if (sel == 1'b0) muxOut = inp1;
    else muxOut = inp2;
  end
endmodule

module mux2to1_3bit (input [2:0] inp1, input [2:0] inp2, input sel, output reg [2:0] muxOut);
  always @ (inp1, inp2, sel)
  begin
    if (sel == 1'b0) muxOut = inp1;
    else muxOut = inp2;
  end
endmodule

module mux2to1_4bit (input [3:0] inp1, input [3:0] inp2, input sel, output reg [3:0] muxOut);
  always @ (inp1, inp2, sel)
  begin
    if (sel == 1'b0) muxOut = inp1;
    else muxOut = inp2;
  end
endmodule

module mux2to1_29bit (input [28:0] inp1, input [28:0] inp2, input sel, output reg [28:0] muxOut);
  always @ (inp1, inp2, sel)
  begin
    if (sel == 1'b0) muxOut = inp1;
    else muxOut = inp2;
  end
endmodule

module mux2to1_39bit (input [38:0] inp1, input [38:0] inp2, input sel, output reg [38:0] muxOut);
  always @ (inp1, inp2, sel)
  begin
    if (sel == 1'b0) muxOut = inp1;
    else muxOut = inp2;
  end
endmodule

module mux8to1_128bit (input [127:0] in0, in1, in2, in3, in4, in5, in6, in7, input [2:0] sel, output reg [127:0] muxOut);
  always @ (in0,in1,in2,in3,in4,in5,in6,in7,sel)
  begin
    case(sel)
      3'b000: muxOut = in0;
      3'b001: muxOut = in1;
      3'b010: muxOut = in2;
      3'b011: muxOut = in3;
      3'b100: muxOut = in4;
      3'b101: muxOut = in5;
      3'b110: muxOut = in6;
      3'b111: muxOut = in7;
    endcase
  end
endmodule

module mux8to1_8bit (input [7:0] in0, in1, in2, in3, in4, in5, in6, in7, input [2:0] sel, output reg [7:0] muxOut);
  always @ (in0,in1,in2,in3,in4,in5,in6,in7,sel)
  begin
    case(sel)
      3'b000: muxOut = in0;
      3'b001: muxOut = in1;
      3'b010: muxOut = in2;
      3'b011: muxOut = in3;
      3'b100: muxOut = in4;
      3'b101: muxOut = in5;
      3'b110: muxOut = in6;
      3'b111: muxOut = in7;
    endcase
  end
endmodule

module mux8to1_1bit (input in0, in1, in2, in3, in4, in5, in6, in7, input [2:0] sel, output reg muxOut);
  always @ (in0,in1,in2,in3,in4,in5,in6,in7,sel)
  begin
    case(sel)
      3'b000: muxOut = in0;
      3'b001: muxOut = in1;
      3'b010: muxOut = in2;
      3'b011: muxOut = in3;
      3'b100: muxOut = in4;
      3'b101: muxOut = in5;
      3'b110: muxOut = in6;
      3'b111: muxOut = in7;
    endcase
  end
endmodule

module mux16to1_8bit (input [7:0] outR0,outR1,outR2,outR3,outR4,outR5,outR6,outR7,outR8,outR9,outR10,outR11,outR12,outR13,outR14,outR15,
                      input [3:0] Sel, output reg [7:0] outBus);
	always@(outR0 or outR1 or outR2 or outR3 or outR4 or outR5 or outR6 or outR7 or outR8 or outR9 or outR10 or outR11 or outR12 or outR13
	        or outR14 or outR15 or Sel)
	begin
	case (Sel)
				4'b0000: outBus=outR0;
				4'b0001: outBus=outR1;
				4'b0010: outBus=outR2;
				4'b0011: outBus=outR3;
				4'b0100: outBus=outR4;
				4'b0101: outBus=outR5;
				4'b0110: outBus=outR6;
				4'b0111: outBus=outR7;
				4'b1000: outBus=outR8;
				4'b1001: outBus=outR9;
				4'b1010: outBus=outR10;
				4'b1011: outBus=outR11;
				4'b1100: outBus=outR12;
				4'b1101: outBus=outR13;
				4'b1110: outBus=outR14;
				4'b1111: outBus=outR15;
	endcase
	end
endmodule

module comparator_29bit (input [28:0] inp1, input [28:0] inp2, output reg compOut);
  always @ (inp1, inp2)
  begin
    if (inp1 == inp2) compOut = 1'b1;
    else compOut = 1'b0;
  end
endmodule

module encoder8to3 (input[7:0] in, output reg [2:0] out,	output reg valid);
always @(in)
begin
  case(in)
    8'b00000001:    begin out = 3'b000; valid=1; end
    8'b00000010:    begin out = 3'b001; valid=1; end
    8'b00000100:    begin out = 3'b010; valid=1; end
    8'b00001000:    begin out = 3'b011; valid=1; end
    8'b00010000:    begin out = 3'b100; valid=1; end
    8'b00100000:    begin out = 3'b101; valid=1; end
    8'b01000000:    begin out = 3'b110; valid=1; end
    8'b10000000:    begin out = 3'b111; valid=1; end
    8'b00000000:    begin out = 3'b000; valid=0; end 
  endcase
end
endmodule

module prio_encoder8to3(input [7:0] in, output reg [2:0] out);
  always @ (in)
  begin
    casex(in)
      8'b1xxxxxxx : out = 3'd0;
      8'b01xxxxxx : out = 3'd1;
      8'b001xxxxx : out = 3'd2;
      8'b0001xxxx : out = 3'd3;
      8'b00001xxx : out = 3'd4;
      8'b000001xx : out = 3'd5;
      8'b0000001x : out = 3'd6;
      8'b00000001 : out = 3'd7;
      default     : out = 3'd0;
    endcase
  end
endmodule

module tristate_29bit (input [28:0] in, input sel, output reg [28:0] out);
  always @ (in, sel)
  if (sel == 1'b1) out = in;
  else out = 29'bz;
endmodule

module tristate_128bit (input [127:0] in, input sel, output reg [127:0] out);
  always @ (in, sel)
  if (sel == 1'b1) out = in;
  else out = 128'bz;
endmodule

module tristate_1bit (input in, input sel, output reg out);
  always @ (in, sel)
  if (sel == 1'b1) out = in;
  else out = 1'bz;
endmodule

module tristate_3bit (input [2:0] in, input sel, output reg [2:0] out);
  always @ (in, sel)
  if (sel == 1'b1) out = in;
  else out = 3'bz;
endmodule

module tristate_8bit (input [7:0] in, input sel, output reg [7:0] out);
  always @ (in, sel)
  if (sel == 1'b1) out = in;
  else out = 8'bz;
endmodule

module tristate_39bit (input [38:0] in, input sel, output reg [38:0] out);
  always @ (in, sel)
  if (sel == 1'b1) out = in;
  else out = 39'bz;
endmodule

module cache_topmodule (input clk, reset,input chooseOffset, input FSM, input [3:0] writeOffset, input writeLRU,
                       input writeReg, input mruWrite, input L2Write, input way_dec_mux_sel,
							  input [28:0] tag, input [4:0] index, input [3:0] offset, input CPUWriteSignal, input CPURead, 
							  input [7:0] busDataIn, input writeValid, output [7:0] cacheDataOut, output L2ToWrite, output [127:0] bigblock, output hit1c, output hit2c);
  // Basic Decoders
  wire [31:0] decodedIndex;
  wire [15:0] decodedOffset;
  wire [7:0] decodedMru;
  
  // Cache way stuff
  wire validWriteData, dirtyWriteData;
  wire [28:0] tagWriteData;
  wire [7:0] blockWriteData;
  
  
  wire way0_valid, way1_valid, way2_valid, way3_valid, way4_valid, way5_valid, way6_valid, way7_valid;
  wire way0_dirty, way1_dirty, way2_dirty, way3_dirty, way4_dirty, way5_dirty, way6_dirty, way7_dirty;
  wire [28:0] way0_tag, way1_tag, way2_tag, way3_tag, way4_tag, way5_tag, way6_tag, way7_tag;
  wire [7:0] way0_data, way1_data, way2_data, way3_data, way4_data, way5_data, way6_data, way7_data;
  wire [127:0] way0_bigblock, way1_bigblock, way2_bigblock, way3_bigblock, way4_bigblock, way5_bigblock, way6_bigblock, way7_bigblock;
  

  // MRU array stuff
  wire [2:0] mru, mruWriteData;
  //wire mruWrite;
  
  // Cycle 1 stuff
  wire c1_valid, c1_dirty;
  wire [28:0] c1_tag;
  wire [7:0] c1_data;
  
  // Mux c1 stuff
  wire mux_c1_valid, mux_c1_dirty;
  wire [28:0] mux_c1_tag;
  wire [7:0] mux_c1_data;
  
  // Comparators
  wire comp0_out, comp1_out, comp2_out, comp3_out, comp4_out, comp5_out, comp6_out, comp7_out;
  wire comp0_outn,comp1_outn, omp2_outn,comp3_outn,comp4_outn,comp5_outn,comp6_outn,comp7_outn;
  
  // Priority Encoder
  wire [2:0] prioEncOut;
  
  // MRU/Prio Enc mux
  wire [2:0] lru_mru;
  
  // LRU circuit
  wire [2:0] lruWayOut;
  wire [7:0] decodedLruWay;
 
  wire L2ToWriteDummy;
  
  // offset mux
  wire [3:0] finalOffset;
  
  assign tagWriteData = tag;
  assign validWriteData = 1'b1;
  assign dirtyWriteData = CPUWriteSignal;
  assign blockWriteData = busDataIn;
  
  assign mruWriteData = prioEncOut;
  
  mux2to1_4bit offsetSelector(offset, writeOffset, chooseOffset, finalOffset);
  decoder5to32 indexDecoder(index, decodedIndex);
  decoder4to16 offsetDecoder(finalOffset, decodedOffset);
  
  cache_way0 cw0 (clk, reset, (writeValid && decodedLruWay[0]), (writeReg && decodedLruWay[0]), (writeValid && decodedLruWay[0]), (writeReg  && decodedLruWay[0]), decodedIndex,
                 decodedOffset, validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                 way0_valid, way0_dirty, way0_tag, way0_data, way0_bigblock);
  cache_way cw1 (clk, reset, (writeValid && decodedLruWay[1]), (writeReg && decodedLruWay[1]), (writeValid && decodedLruWay[1]), (writeReg  && decodedLruWay[1]), decodedIndex,
                 decodedOffset, validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                 way1_valid, way1_dirty, way1_tag, way1_data, way1_bigblock);
  cache_way cw2 (clk, reset, (writeValid && decodedLruWay[2]), (writeReg && decodedLruWay[2]), (writeValid && decodedLruWay[2]), (writeReg && decodedLruWay[2]), decodedIndex,
                 decodedOffset, validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                 way2_valid, way2_dirty, way2_tag, way2_data, way2_bigblock);
  cache_way cw3 (clk, reset, (writeValid && decodedLruWay[3]), (writeReg && decodedLruWay[3]), (writeValid && decodedLruWay[3]), (writeReg && decodedLruWay[3]), decodedIndex,
                 decodedOffset, validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                 way3_valid, way3_dirty, way3_tag, way3_data, way3_bigblock);
  cache_way cw4 (clk, reset, (writeValid && decodedLruWay[4]), (writeReg && decodedLruWay[4]), (writeValid && decodedLruWay[4]), (writeReg && decodedLruWay[4]), decodedIndex,
                 decodedOffset, validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                 way4_valid, way4_dirty, way4_tag, way4_data, way4_bigblock);
  cache_way cw5 (clk, reset, (writeValid && decodedLruWay[5]), (writeReg && decodedLruWay[5]), (writeValid && decodedLruWay[5]), (writeReg && decodedLruWay[5]), decodedIndex,
                 decodedOffset, validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                 way5_valid, way5_dirty, way5_tag, way5_data, way5_bigblock);
  cache_way cw6 (clk, reset, (writeValid && decodedLruWay[6]), (writeReg && decodedLruWay[6]), (writeValid && decodedLruWay[6]), (writeReg && decodedLruWay[6]), decodedIndex,
                 decodedOffset, validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                 way6_valid, way6_dirty, way6_tag, way6_data, way6_bigblock);
  cache_way cw7 (clk, reset, (writeValid && decodedLruWay[7]), (writeReg && decodedLruWay[7]), (writeValid && decodedLruWay[7]), (writeReg && decodedLruWay[7]), decodedIndex,
                 decodedOffset, validWriteData, dirtyWriteData, tagWriteData, blockWriteData,
                 way7_valid, way7_dirty, way7_tag, way7_data, way7_bigblock);
                 
  mru_array mru_arr(clk, reset, mruWrite, decodedIndex, mruWriteData, mru);
  decoder3to8 mruDecoder(mru, decodedMru);
  
  mux8to1_8bit cacheoutmux(way0_data,way1_data,way2_data,way3_data,way4_data,way5_data,way6_data,way7_data, prioEncOut, cacheDataOut);

  mux8to1_1bit cachedirtymux(way0_dirty,way1_dirty,way2_dirty,way3_dirty,way4_dirty,way5_dirty,way6_dirty,way7_dirty, prioEncOut, L2ToWriteDummy);
  
  and (L2ToWrite, L2ToWriteDummy, L2Write);
  
  tristate_39bit t0({way0_valid, way0_dirty, way0_tag, way0_data}, decodedMru[0] || FSM, {c1_valid, c1_dirty, c1_tag, c1_data});
  tristate_39bit t1({way1_valid, way1_dirty, way1_tag, way1_data}, decodedMru[1] || FSM, {c1_valid, c1_dirty, c1_tag, c1_data});
  tristate_39bit t2({way2_valid, way2_dirty, way2_tag, way2_data}, decodedMru[2] || FSM, {c1_valid, c1_dirty, c1_tag, c1_data});
  tristate_39bit t3({way3_valid, way3_dirty, way3_tag, way3_data}, decodedMru[3] || FSM, {c1_valid, c1_dirty, c1_tag, c1_data});
  tristate_39bit t4({way4_valid, way4_dirty, way4_tag, way4_data}, decodedMru[4] || FSM, {c1_valid, c1_dirty, c1_tag, c1_data});
  tristate_39bit t5({way5_valid, way5_dirty, way5_tag, way5_data}, decodedMru[5] || FSM, {c1_valid, c1_dirty, c1_tag, c1_data});
  tristate_39bit t6({way6_valid, way6_dirty, way6_tag, way6_data}, decodedMru[6] || FSM, {c1_valid, c1_dirty, c1_tag, c1_data});
  tristate_39bit t7({way7_valid, way7_dirty, way7_tag, way7_data}, decodedMru[7] || FSM, {c1_valid, c1_dirty, c1_tag, c1_data});
  
  mux8to1_128bit bigblockmux(way0_bigblock, way1_bigblock, way2_bigblock, way3_bigblock, way4_bigblock, way5_bigblock, way6_bigblock, way7_bigblock, prioEncOut, bigblock);
  
  mux2to1_39bit mux_c1_select({c1_valid, c1_dirty, c1_tag, c1_data}, {way7_valid, way7_dirty, way7_tag, way7_data},
                              FSM, {mux_c1_valid, mux_c1_dirty, mux_c1_tag, mux_c1_data});
  
  comparator_29bit comp0(tag, way0_tag, comp0_out);
  comparator_29bit comp1(tag, way1_tag, comp1_out);
  comparator_29bit comp2(tag, way2_tag, comp2_out);
  comparator_29bit comp3(tag, way3_tag, comp3_out);
  comparator_29bit comp4(tag, way4_tag, comp4_out);
  comparator_29bit comp5(tag, way5_tag, comp5_out);
  comparator_29bit comp6(tag, way6_tag, comp6_out);
  comparator_29bit comp7(tag, mux_c1_tag, comp7_out);
  
  assign comp0_outn = comp0_out && way0_valid;
  assign comp1_outn = comp1_out && way1_valid;
  assign comp2_outn = comp2_out && way2_valid;
  assign comp3_outn = comp3_out && way3_valid;
  assign comp4_outn = comp4_out && way4_valid;
  assign comp5_outn = comp5_out && way5_valid;
  assign comp6_outn = comp6_out && way6_valid;
  assign comp7_outn = comp7_out && mux_c1_valid;
  
  prio_encoder8to3 penc8to3({comp0_outn,comp1_outn, comp2_outn,comp3_outn,comp4_outn,comp5_outn,comp6_outn,comp7_outn}, prioEncOut);
  
  wire [2:0] way_dec_mux_out;
  
  mux2to1_3bit way_decoder_input_mux(prioEncOut, lruWayOut, way_dec_mux_sel, way_dec_mux_out);
  
  decoder3to8 lruWayDecoder(way_dec_mux_out, decodedLruWay);
  
  mux2to1_3bit mux_mru_prioenc(mru, prioEncOut, FSM, lru_mru);
  
  LRUCounter lruc(clk,lru_mru,index,{comp0_outn||comp1_outn||comp2_outn||comp3_outn||comp4_outn||comp5_outn||comp6_outn||comp7_outn},
                  reset,writeLRU,{way7_valid,way6_valid,way5_valid,way4_valid,way3_valid,way2_valid,way1_valid,way0_valid},
                  lruWayOut);
  assign hit1c = comp7_outn;
  assign hit2c = comp0_outn||comp1_outn||comp2_outn||comp3_outn||comp4_outn||comp5_outn||comp6_outn||comp7_outn;
endmodule

