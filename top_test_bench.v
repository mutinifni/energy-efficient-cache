`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:19:58 04/14/2016
// Design Name:   TopModule
// Module Name:   E:/WORKSPACE TA/way_cache_integration/top_test_bench.v
// Project Name:  way_cache_integration
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: TopModule
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module top_test_bench;

	// Inputs
	reg clk;
	reg reset;
	reg [41:0] VA_in;
	reg [4:0] protection_bits;
	reg [61:0] data_in;
	reg CPUWriteSignal;
	reg CPURead;
	reg [7:0] busDataIn;

	// Outputs
	wire [7:0] cacheDataOut;
	wire isValid;
	wire L2ToWrite;
	wire [127:0] bigblock;
	wire exception;

	// Instantiate the Unit Under Test (UUT)
	TopModule uut (
		.clk(clk), 
		.reset(reset), 
		.VA_in(VA_in), 
		.protection_bits(protection_bits), 
		.data_in(data_in), 
		.CPUWriteSignal(CPUWriteSignal), 
		.CPURead(CPURead), 
		.busDataIn(busDataIn), 
		.cacheDataOut(cacheDataOut), 
		.isValid(isValid), 
		.L2ToWrite(L2ToWrite), 
		.bigblock(bigblock),
		.exception(exception)
	);
always
	#5 clk=~clk;
	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 1;
		VA_in = 0;
		data_in = 0;
		CPUWriteSignal = 0;
		CPURead = 0;
		busDataIn = 0;

		// Wait 100 ns for global reset to finish
		#11 // tlb hit read hit
		protection_bits = 5'd1;
				reset=0;
				VA_in = 42'hf15049e02;
				CPURead = 1;

		
		#59 // tlb miss write hit
		VA_in = 42'b100010101000010001000100010001_111_00000_0010;
		protection_bits = 5'd1;
		data_in = 62'b1_100010101000010001000100010001_11111111111111111111111111_01000;
   CPUWriteSignal = 1;
   busDataIn = 20;
   CPURead = 1'b0;
			
		#80 //tlb hit read hit
		VA_in = 42'b100010101000010001000100010001_111_00000_0010;
		protection_bits = 5'd1;
		CPUWriteSignal = 0;
   busDataIn = 0;
   CPURead = 1'b1;
		
		#50 // tlb miss read miss 
		VA_in = 42'b111110101000010001000100010001_111_00001_0111;
		protection_bits = 5'd1;
		data_in = 62'b1_111110101000010001000100010001_11111111111111111111111001_01000;
   CPUWriteSignal = 0;
   busDataIn = 0;
   CPURead = 1'b1;
		
	#50;
	#10
   //FSM state
   ;
   
   #10
   ;
   
   #10 
   busDataIn=0;
   
   #10
   busDataIn=1;
   
   #10
   busDataIn=2;
   
   #10
   busDataIn=3;
   
   #10
   busDataIn=4;
   
   #10
   busDataIn=5;
   
   #10
   busDataIn=6;
   
   #10
   busDataIn=7;
   
   #10
   busDataIn=8;
   
   #10
   busDataIn=9;
   
   #10
   busDataIn=10;
   
   #10
   busDataIn=11;
   
   #10
   busDataIn=12;
   
   #10
   busDataIn=13;
   
   #10
   busDataIn=14;
   
   #10
   busDataIn=15;
 
   #10
   ;
	// tlb hit read hit again with correct MRU
	
	#70 //tlb miss read hit
	VA_in = 42'b100010101000011111000100010001_001_00001_0010;
		protection_bits = 5'd3;
		CPUWriteSignal = 0;
		data_in = 62'b1_100010101000011111000100010001_00000000000000000000000000_01000;
   busDataIn = 0;
   CPURead = 1'b1;
	#80
	// tlb hit write miss
	         
	VA_in = 42'b100010101000011111000100011101_111_00001_0101;
		protection_bits = 5'd1;
	//	data_in = 62'b1_111110101000010001000100010001_11111111111111111111000010_01000;
   CPUWriteSignal = 1;
   busDataIn = 0;
   CPURead = 1'b0;
	
	#30
   #10
   //FSM state
   ;
   
   #10
   ;
   
   #10
   busDataIn=15;
   
   #10
   busDataIn=14;
   
   #10
   busDataIn=13;
   
   #10
   busDataIn=12;
   
   #10
   busDataIn=11;
   
   #10
   busDataIn=10;
   
   #10
   busDataIn=9;
   
   #10
   busDataIn=8;
   
   #10
   busDataIn=7;
   
   #10
   busDataIn=6;
   
   #10
   busDataIn=5;
   
   #10
   busDataIn=4;
   
   #10
   busDataIn=3;
   
   #10
   busDataIn=2;
   
   #10
   busDataIn=1;
   
   #10
   busDataIn=0;
   
   #10
   ;
	
   #20
	VA_in = 42'b100010101000010001000100010001_111_00000_0010;
		protection_bits = 5'd11;
		CPUWriteSignal = 0;
   busDataIn = 0;
   CPURead = 1'b1;

	#30 $finish;
		// Add stimulus here

	end
      
endmodule

